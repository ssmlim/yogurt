﻿Ext.define('Yogurt.view.Find', {
    extend: 'Ext.Container',
    xtype: 'find',
    config: {
        fullscreen: true,
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype: 'toolbar', title: '비밀번호 찾기' },
            { xtype : 'find_password', flex: 1 }
        ]
    }
});