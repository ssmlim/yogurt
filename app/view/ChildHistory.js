Ext.define('Yogurt.view.ChildHistory', {
    extend: 'Ext.Container',
    xtype: 'child_history',
    config: {
        layout: {
            type: 'fit'
        },

        items: [
            { xtype : 'child_history_list' },
            {
                xtype : 'toolbar',
                docked : 'top',
                title : '검사 기록 조회',
                items : [
                    {
                        text : '뒤로',
                        action : 'HistoryCancel',
                        cls : 'mainbutton'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items :
                    [
                        {
                            flex : 5,
                            text : '일반검사',
                            id : 'NO',
                            cls : 'GrayTypeButton'
                        },
                        {
                            flex : 5,
                            text : '정밀검사',
                            id : 'DE',
                            cls : 'GrayTypeButton'
                        }
                    ]
            },
        ]
    }
});