﻿Ext.define('Yogurt.view.Join', {
    extend: 'Ext.Container',
    xtype: 'join',
    config: {
        fullscreen: true,
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype: 'toolbar', title: '회원가입' },
            { xtype : 'memberjoin', flex: 7 }
        ]
    }
});