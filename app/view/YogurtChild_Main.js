﻿Ext.define('Yogurt.view.YogurtChild_Main', {
    extend: 'Ext.navigation.View',
    xtype: 'yogurtchild_main',
    config: {
        navigationBar: {
            ui : 'sencha',
            layout: {
                pack: 'center',
                type: 'hbox'
            },
            items: [
                {
                    text: '이전',
                    ui: 'back',
                    align : 'left',
                    action: 'back',
                    hidden : true,
                    cls : 'subbutton'
                },
                {
                    text: '다음',
                    ui: 'forward',
                    align : 'right',
                    action: 'forward',
                    hidden : true,
                    cls : 'subbutton'

                }
            ]
        },
        items : [{
            xtype : 'yogurtchild_testselect'
        }]
    }

});
