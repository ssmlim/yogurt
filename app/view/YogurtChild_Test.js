Ext.define('Yogurt.view.YogurtChild_Test', {
    extend: 'Ext.Container',
    xtype: 'yogurtchild_test',
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype : 'toolbar',
                docked: 'top',
                title : '검사 아동 선택'
            },
            {
                xtype : 'yogurtchild_testchildlist'
            }
        ]
    }
});