Ext.define('Yogurt.view.YogurtChild_Chart', {
    extend: 'Ext.Container',
    xtype: 'yogurtchild_chart',
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
              xtype: 'toolbar',
              docked: 'top',
              title: '검사 결과',
                items : [
                    {
                        text : '뒤로',
                        action : 'ChartCancel',
                        cls : 'mainbutton'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items : [
                    {
                        text : '항목 보기',
                        action : 'ResultList',
                        cls : 'GrayTypeButton',
                        flex : 3
                    },
                    {
                        text : '그래프 보기',
                        action : 'Graph',
                        cls : 'GrayTypeButton',
                        flex : 3
                    },
                    {
                        text : '메일로 전송하기',
                        action : 'Mail',
                        cls : 'GrayTypeButton',
                        flex : 3
                    }
                ]
            },
            {
                xtype : 'answerlist',
                hidden : true
            },
            {
                xtype : 'yogurtchild_chartview'
            }
        ]
    }
});

