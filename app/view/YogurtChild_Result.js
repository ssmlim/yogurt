Ext.define('Yogurt.view.YogurtChild_Result', {
    extend: 'Ext.Container',
    xtype: 'yogurtchild_result',
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items :
                [
                    {
                        flex : 2.5,
                        text : '항목 보기',
                        action : 'TestResultList',
                        cls : 'GrayTypeButton'
                    },
                    {
                        flex : 2.5,
                        text : '그래프 보기',
                        action : 'TestResultGraph',
                        cls : 'GrayTypeButton'
                    },
                    {
                        flex : 2.5,
                        text : '검사 종료',
                        action : 'ChartCancel',
                        cls : 'GrayTypeButton'
                    },
                    {
                        flex : 2.5,
                        text : '메일로 전송하기',
                        action : 'ResultSendMail',
                        cls : 'GrayTypeButton'
                    }
                ]
            },
            {
                xtype : 'answerlist',
                hidden : true
            },
            {
                xtype: 'yogurtchild_resultview'
            }
        ]
    }
});

