Ext.define('Yogurt.view.ChildDetail', {
    extend: 'Ext.Container',
    xtype: 'child_detail',
    config: {
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype : 'childdetail_form'}
        ]
    }
});