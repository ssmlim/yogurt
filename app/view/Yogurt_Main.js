﻿Ext.define('Yogurt.view.Yogurt_Main', {
    extend: 'Ext.navigation.View',
    xtype: 'yogurt_main',
    id : 'Yogurt_Main',

    config: {
        autoDestroy: true,
        navigationBar: {
        ui : 'plain',
            items: [
                {
                    text: '홈',
                    hidden : true,
                    action: 'goHome',
                    cls : 'mainbutton'
                },
                {
                    text: '정보수정',
                    action: 'UserEdit',
                    cls : 'mainbutton'
                },
                {
                    text: '로그아웃',
                    action: 'LogOut',
                    cls : 'mainbutton'
                },
                {
                    text: '아동추가',
                    align : 'right',
                    hidden : true,
                    action: 'ChildAdd',
                    cls : 'mainbutton'

                },
                {
                    text: '아동관리',
                    align : 'right',
                    action: 'ChildList',
                    cls : 'mainbutton'
                },
                {

                    iconCls : 'info',
                    align : 'right',
                    action: 'Help',
                    cls : 'mainbutton'

                }
            ]
        },
        items : [{
            xtype : 'yogurt_mainpanel'
        }]
    }

});



