﻿Ext.define('Yogurt.view.ChildInfo', {
    extend: 'Ext.Container',
    xtype: 'childinfo',
    config: {
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype : 'toolbar', docked: 'top', title : '아이 관리'},
            { xtype : 'childinfo_list', hidden : true , flex :10},
            { hidden : true , flex :10, styleHtmlContent: true, scrollable: true , html : '아동정보가 없습니다.<br>상단의 아동추가 버튼을 눌러 정보를 추가해주세요.' }
        ]
    }
});