Ext.define('Yogurt.view.YogurtChild_NonAnswer', {
    extend: 'Ext.Panel',
    xtype: 'yogurtchild_nonanswer',
    config: {
        layout: {
            type: 'fit'
        },
        items: [
            { xtype : 'yogurtchild_nonanswerlist'},
            {
                xtype : 'toolbar',
                docked: 'bottom',
                    items : [
                        {
                            width : '100%',
                            text : '그만하기',
                            action : 'ExitTest',
                            cls : 'GrayTypeButton'
                        }
                    ]
            }
        ]
    }
});
