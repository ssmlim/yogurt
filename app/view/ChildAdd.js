﻿Ext.define('Yogurt.view.ChildAdd', {
    extend: 'Ext.Container',
    xtype: 'childadd_form',
    config: {
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype : 'childadd_form' }
        ]
    }
});