﻿Ext.define('Yogurt.view.Help', {
    extend: 'Ext.Panel',
    xtype: 'help',
    id : 'helppanel',
    config: {
        layout : 'fit',
        modal: true,
        floating: true,
        hideOnMaskTap: true,
        hidden: true,
        left: 0,
        width: '95%',
        height: '70%',
        items: [
            {
                xtype: 'help_panel'
            }
        ]
    }
});

