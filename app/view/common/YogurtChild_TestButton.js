﻿Ext.define('Yogurt.view.common.YogurtChild_TestButton', {
    extend: 'Ext.Panel',
    xtype: 'yogurtchild_testbutton',
    //style: 'background-color: #FFFFFF;',
    config: {
        layout:{
            type: 'hbox',
            align : 'middle'
        },
        items: [
            {
                xtype : 'image',
                src: '../../Yogurt/resources/icons/checkBtn_1.png',
                itemId :'goodresult',
                flex: 3,
                height: 100,
                width: 100,
                id : 'answerGood'
            },
            {
                xtype : 'image',
                src: '../../Yogurt/resources/icons/checkBtn_2.png',
                itemId :'normalresult',
                flex: 3,
                height: 100,
                width: 100,
                id : 'answerNormal'
            },
            {
                xtype : 'image',
                src: '../../Yogurt/resources/icons/checkBtn_3.png',
                itemId :'badresult',
                flex: 3,
                height: 100,
                width: 100,
                id : 'answerBad'

            }
            ]
    }
});