Ext.define('Yogurt.view.common.NoticeList', {
    extend: 'Ext.List',
    xtype: 'noticelist',
    config: {
        store: {
            xclass: 'Yogurt.store.GetNoticeStore'
        },

        itemHeight : 30,
        itemTpl: "<table style='margin:0 auto;' width='100%'>" +
            "<tr><td width='70%'>{Ntitle}</td>" +
            "<td width='30%' style='text-align:center;'>[{Ndate}]</td>" +
            "</tr></table>"
    }

});


