Ext.define('Yogurt.view.common.ChildAdd_Form', {
    xtype: 'childadd_form',
    extend: 'Ext.form.FormPanel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Radio',
        'Ext.field.DatePicker',
        'Ext.TitleBar'
    ],
    config: {
        autoDestroy: true,
        items: [
            {
                xtype : 'toolbar',
                docked: 'top',
                title : '아동 추가'
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '30%'
                },
                items: [
                    {
                        xtype: 'textfield',
                        id: 'AChildName',
                        name: 'ChildName',
                        label: '이름',
                        placeHolder: '이름',
                        clearIcon: true
                    },
                    {
                        xtype: 'datepickerfield',
                        id: 'AChildBirth',
                        name: 'ChildBirth',
                        label: '생일',
                        value : new Date(),
                        dateFormat: 'Y-m-d'
                    },

                            {
                                xtype: 'radiofield',
                                labelWidth: '30%',
                                id: 'AChildSex1',
                                name: 'ChildSex',
                                value: '여자',
                                label: '여자'
                            },
                            {
                                xtype: 'radiofield',
                                labelWidth: '30%',
                                id: 'AChildSex2',
                                name: 'ChildSex',
                                value: '남자',
                                label: '남자'
                            }

                ]
            },

                    { xtype: 'button', text: '저장', margin : '10px' ,action: 'ChildAddOk', cls : 'PinkTypeButton' },
                    { xtype: 'button', text: '취소', margin : '10px' ,action: 'ChildCancel',cls : 'PinkTypeButton'}


        ]
    }
});
