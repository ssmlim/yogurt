Ext.define('Yogurt.view.common.YogurtChild_TestMain', {
    extend: 'Ext.Panel',
    xtype: 'yogurtchild_testmain',
    config: {
        layout: 'vbox',
        scrollable : 'false',
        items: [
            { xtype : 'yogurtchild_testquestion',flex : 6},
            { xtype : 'yogurtchild_testbutton', flex : 3 },
            { xtype : 'toolbar',  flex : 1, docked : 'bottom',
                items : [
                    {
                        text : '남은 문항 보기',
                        action : 'NonAnswer',
                        flex : 5
                    , cls : 'GrayTypeButton'
                    },
                    {
                        text : '그만하기',
                        action : 'ExitTest',
                        flex : 5
                    , cls : 'GrayTypeButton'
                    }
                ]
            }
        ]
    }
});