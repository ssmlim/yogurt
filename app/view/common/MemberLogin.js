Ext.define('Yogurt.view.common.MemberLogin', {
    extend: 'Ext.form.Panel',
    xtype: 'memberlogin',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Password',
        'Ext.field.Email'
    ],

    config: {
        cls : 'memberlogin',
        scrollable: 'false',
        items: [
            {
                xtype: 'fieldset',
                id: 'loginfieldset',

                instructions: '아이디는 E-mail형식으로 입력!!',
                defaults: {
                    labelWidth: '30%'
                },
                items: [
                    {
                        xtype: 'emailfield',
                        id: 'loginID',
                        itemId: 'loginid',
                        label: '아이디',
                        autoCapitalize: true,
                        clearIcon: true
                    },
                    {
                        xtype: 'passwordfield',
                        id: 'loginPWD',
                        itemId: 'loginpwd',
                        label: '비밀번호'

                    }
                    /*
                     {
                     xtype: 'checkboxfield',
                     id : 'check',
                     itemId : 'logincheckbox',
                     label: '기억하기',
                     labelWidth: '82%',
                     style : 'font-size : 15px; text-align : center; background-color:#f7f7f7'
                     }
                     */
                ]
            },
            {xtype: 'button', text: '로그인', margin: '10px', action: 'login', cls : 'GrayTypeButton' },
            {xtype: 'button', text: '비밀번호 찾기', margin: '10px', action: 'findpw', cls : 'GrayTypeButton'},
            {xtype: 'button', text: '회원가입', margin: '10px', action: 'join', cls : 'GrayTypeButton'}
        ]
    }
});
