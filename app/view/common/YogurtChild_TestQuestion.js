Ext.define('Yogurt.view.common.YogurtChild_TestQuestion', {
    extend: 'Ext.DataView',
    xtype: 'yogurtchild_testquestion',
    config: {
                scrollable : false,
                height : '100%',
                itemTpl: "<div style='text-align:center;position: absolute; top: 50%; display: block; width: 100%;'><b>{Qnum} . {Qask}</b></div>",
                store : {
                    xclass : 'Yogurt.store.QuestionStore'
                }

    }
});