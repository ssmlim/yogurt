Ext.define('Yogurt.view.common.SearchChildView', {
    extend: 'Ext.carousel.Carousel',
    xtype: 'searchchildview',
    config: {
        defaults: {
            styleHtmlContent: true
        },
        direction: 'horizontal',
        indicator : false,
        maxHeight : '70px',
        items : [],
        delay: 3000,
        start: true,
        listeners: {
            tap: {
                fn: function() {
                    this.pause();
                },
                element: 'element'
            },
            swipe: {
                fn: function() {
                    this.start();
                },
                element: 'innerElement'
            }
        }
    },
    initialize: function() {
        if (this.config.start) {
            this.start();
        }

        var me = this;

        me.element.on({
            scope : me,
            tap   : function(e, t) {
                me.fireEvent('tap', me, me.getActiveItem(), e, t);
                this.start();
            }
        });

        me.callParent(arguments);
    },
    rotate: function() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        if (this.getActiveIndex() === this.getMaxItemIndex()) {
            this.setActiveItem(0, 'fade');
        }
        else {
            this.next();
        }
        this.timeout = Ext.defer(this.rotate, this.config.delay, this);
    },
    start: function(delayStart) {
        this.timeout = Ext.defer(this.rotate, delayStart || this.config.delay, this);
    },
    pause: function(delayStart) {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        if (delayStart) {
            this.start(delayStart);
        }
        return this;
    },
    stop: function(delayStart) {
        this.pause(delayStart);
        this.setActiveItem(0, 'fade');
        return this;
    }



});