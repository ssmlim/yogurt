Ext.define('Yogurt.view.common.AnswerListView', {
    extend: 'Ext.List',
    xtype : 'answerlist',
    config: {
        itemTpl: '{Qask} {Aanswer}',
        store : {
            xclass : 'Yogurt.store.GetAnswerStore'
        },
        striped: true,
        grouped : true

    }
});
