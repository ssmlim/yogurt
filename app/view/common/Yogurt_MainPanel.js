Ext.define('Yogurt.view.common.Yogurt_MainPanel', {
    extend: 'Ext.Container',
    xtype: 'yogurt_mainpanel',
    config: {
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype : 'spacer' },
            {
                xtype : 'image',
                src: '../../Yogurt/resources/icons/logo_gray.png',
                itemId :'logoBtn',
                flex: 3,
                height: '100%',
                width: '100%'
            },
            { xtype : 'yogurt_maintestselect', flex: 7 },
            { xtype : 'notice', height: '130px' }
        ]
    }
});