Ext.define('Yogurt.view.common.UserEdit_Form', {
    xtype: 'useredit_form',
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    config: {
        items: [
            {
                xtype : 'toolbar',
                docked: 'top',
                title : '회원정보 수정'
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '30%'
                },
                items: [
                    {
                        xtype: 'emailfield',
                        id: 'editId',
                        label: '아이디(E-mail)',
                        autoCapitalize: true,
                        disabled: true
                    },
                    {
                        xtype: 'textfield',
                        id: 'editName',
                        label: '이름',
                        disabled: true
                    },
                    {
                        id: 'editPhone',
                        xtype: 'textfield',
                        name: 'phone',
                        label: '휴대폰번호',
                        autoCapitalize: true,
                        clearIcon: true
                    }
                ]
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '30%'
                },
                items : [
                    {
                        xtype: 'passwordfield',
                        id: 'editPassword',
                        label: '비밀번호',
                        required : true

                    },
                    {
                        xtype: 'passwordfield',
                        id: 'editNewpassword',
                        label: '새 비밀번호',
                        placeHolder : '비밀번호 변경시에만 입력해주세요'
                    },
                    {
                        xtype: 'passwordfield',
                        id: 'editNewpasswordcheck',
                        label: '새 비밀번호 확인',
                        placeHolder : '비밀번호 변경시에만 입력해주세요'
                    }
                ]
            },

                    {
                       xtype : 'button', text: '수정', margin: '10px', action: 'UserEditOk', cls : 'PinkTypeButton'
                    },
                    {
                        xtype : 'button',text: '취소', margin: '10px', action: 'UserEditCancel', cls : 'PinkTypeButton'
                    }


        ]
    }
});
