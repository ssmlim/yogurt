Ext.define('Yogurt.view.common.ChildDetail_Form', {
    xtype: 'childdetail_form',
    extend: 'Ext.form.FormPanel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Radio',
        'Ext.field.DatePicker',
        'Ext.TitleBar'
    ],
    config: {
        items: [
            {
                xtype : 'toolbar',
                docked: 'top',
                title : '아동정보 수정'
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '30%'
                },
                items: [

                    {
                        xtype: 'textfield',
                        id: 'DCnum',
                        name: 'Cnum',
                        label: 'Cnum',
                        required: true,  // 필수입력항목 표시
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        id: 'DChildName',
                        name: 'ChildName',
                        label: '이름',
                        placeHolder: '이름',
                        clearIcon: true
                    },
                    {
                        xtype: 'datepickerfield',
                        id: 'DChildBirth',
                        name: 'ChildBirth',
                        label: '생일',
                        dateFormat: 'Y-m-d'
                    },

                            {
                                xtype: 'radiofield',
                                labelWidth: '30%',
                                id: 'DChildSex1',
                                name: 'ChildSex',
                                value: '여자',
                                label: '여자'
                            },
                            {
                                xtype: 'radiofield',
                                labelWidth: '30%',
                                id: 'DChildSex2',
                                name: 'ChildSex',
                                value: '남자',
                                label: '남자'
                            }

                ]
            },

                    { xtype: 'button', margin : '10px', text: '저장', action: 'ChildEdit',cls : 'PinkTypeButton' },
                    { xtype: 'button', margin : '10px',text: '취소',  action: 'ChildCancel',cls : 'PinkTypeButton'}

        ]
    }
});
