Ext.define('Yogurt.view.common.YogurtChild_TestChildList', {
    extend: 'Ext.List',
    xtype : 'yogurtchild_testchildlist',
    config: {
        itemTpl: '[ {ChildName} ]  횟수 : {ReportCount}  </font></b>',
        store : {
            xclass : 'Yogurt.store.GetChildHistoryCountStore'
        },
        onItemDisclosure : true,
        striped: true
    }
});
