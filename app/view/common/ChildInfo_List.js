﻿Ext.define('Yogurt.view.common.ChildInfo_List', {
    extend: 'Ext.List',
    xtype: 'childinfo_list',
    config: {
        store: {
            xclass: 'Yogurt.store.GetChildListStore'
        },
        itemTpl: '<table width = "100%"><tr>' +
            '<td width = "55%"><div>{ChildName}</div><div> {ChildBirth}</div></td>'+
            '<td width = "15%"><span class="ChildEdit"><img  width="30px" height="30px" src="./touch/resources/themes/images/default/pictos/settings.png" /></span></td>'+
            '<td width = "15%"><span class="ChildDelete"><img  width="30px" height="30px" src="./touch/resources/themes/images/default/pictos/trash.png" /></span></td>'+
            '<td width = "15%"><span class="ChildHistory"><img  width="30px" height="30px" src="./touch/resources/themes/images/default/pictos/info.png" /></span></td></tr>'
    }

});


