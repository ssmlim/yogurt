Ext.define('Yogurt.view.common.YogurtChild_NonAnswerList', {
    extend: 'Ext.List',
    xtype: 'yogurtchild_nonanswerlist',
    config: {
        itemTpl: '[ {Qnum} 번 ]  답변없음',
        store : {
            xclass : 'Yogurt.store.GetNonAnswerStore'
        }
    }
});
