Ext.define('Yogurt.view.common.YogurtChild_ChartView', {
    extend: 'Ext.Panel',
    xtype: 'yogurtchild_chartview',

    config: {
        layout: 'fit',
        styleHtmlContent : true,

        html: '-ㅅ-'

    }
});