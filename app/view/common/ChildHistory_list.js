Ext.define('Yogurt.view.common.ChildHistory_list', {
    extend: 'Ext.List',
    xtype : 'child_history_list',
    config: {
        itemTpl: '[ {RowNumber}<b><font>회차 ]  날짜 : {Adate} </font></b>',
        store : {
            xclass : 'Yogurt.store.GetChildHistoryStore'
        },
        onItemDisclosure : true,
        striped: true
    }
});