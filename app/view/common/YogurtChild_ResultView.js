Ext.define('Yogurt.view.common.YogurtChild_ResultView', {
    extend: 'Ext.Panel',
    xtype: 'yogurtchild_resultview',

    background: 'white',

    config: {
        layout: 'fit',
        styleHtmlContent : true,

        html: '-ㅅ-'

    }
});