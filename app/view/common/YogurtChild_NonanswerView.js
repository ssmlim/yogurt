Ext.define('Yogurt.view.common.YogurtChild_NonanswerView', {
    extend: 'Ext.List',
    xtype: 'yogurtchild_nonanswerview',
    config: {
        itemTpl: '[ {Qnum} 번 ]  답변없음',
        store : {
            xclass : 'Yogurt.store.GetNonAnswerStore'
        },
        onItemDisclosure : true
    }
});
