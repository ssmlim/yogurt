Ext.define('Yogurt.view.common.NoticeDetail', {
    extend: 'Ext.Panel',
    xtype: 'noticedetail',
    config: {
        layout : 'fit',
        modal: true,
        floating: true,
        hideOnMaskTap: true,
        hidden: true,
        left: 0,
        width: '95%',
        height: '70%',
        items: [
            {
                xtype: 'toolbar',
                docked : 'top',
                title : '제목'
            },
            {
                scrollable : true,
                xtype: 'panel',
                layout:'fit',
                styleHtmlContent : true,
                html : 'ㅇㅅㅇ'
            }
        ]
    }
});







