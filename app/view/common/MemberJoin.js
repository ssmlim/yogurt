var security = '개인정보보호'  //개인정보보호 내용 (html형식으로 작성)

Ext.define('Yogurt.view.common.MemberJoin', {
    xtype: 'memberjoin',
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio',
        'Ext.field.Checkbox'
    ],
    id: 'basicform2',
    config: {
        items: [
            {
                xtype: 'fieldset',
                id: 'fieldset1',
                defaults: {
                    labelWidth: '30%'
                },
                items: [
                    {
                        xtype         : 'emailfield',
                        id          : 'joinId',
                        label         : '아이디(E-mail)',
                        placeHolder   : '다음(한메일) 금지',
                        setvalue         : '',
                        autoCapitalize: true,
                        required      : true,
                        clearIcon     : true
                    },
                    {
                        xtype: 'textfield',
                        id: 'joinName',
                        label : '이름'
                    },
                    {
                        xtype: 'passwordfield',
                        id : 'joinPassword',
                        label: '비밀번호',
                        placeHolder   : '8자이상 입력',
                        required      : true
                    },
                    {
                        xtype: 'passwordfield',
                        id : 'joinPasswordcheck',
                        label: '비밀번호확인',
                        required      : true
                    },
                    {
                        id            : 'joinPhone',
                        xtype         : 'textfield',
                        name          : 'joinPhone',
                        label         : '휴대폰번호',
                        placeHolder   : 'ex) 01012345678',
                        autoCapitalize: true,
                        required      : true,
                        clearIcon     : true
                    },
                    {
                        html: "<iframe src='./app/view/common/PersonalInfoAgreement.htm" +
                                "' width='100%' height='100px' frameborder='0'></iframe>"
                    },
                    {
                        xtype: 'checkboxfield',
                        id: 'joinCheck',
                        label: '위 내용에 따라 정보 제공에 동의합니다.',
                        labelWidth: '82%'

                    }
                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button'
                },
                layout: {
                    type: 'vbox'
                },
                items: [
                    {xtype: 'button',text: '가입',margin: '5px', action: 'join_ok', cls : 'PinkTypeButton'},
                    {xtype: 'button',text: '취소',margin: '5px', action: 'join_cancel', cls : 'PinkTypeButton'}
                ]
            }
        ]
    }
});
