﻿Ext.define('Yogurt.view.common.Notice', {
    extend: 'Ext.Container',
    xtype: 'notice',
    config: {
        layout : 'vbox',

        cls : 'Notice',
        items: [
            {
                xtype : 'searchchildview',
                height : '70px',
                cls : 'SearchList'

            },
            {
              xtype: 'panel',
                html : '',
                height : '70px',
                hidden : true
            },
            {
                xtype: 'noticelist',
                height : '60px',
                cls : 'NoticeList'
            }
        ]
    }
});







