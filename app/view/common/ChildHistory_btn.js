Ext.define('Yogurt.view.common.ChildHistory_btn', {
    extend: 'Ext.Panel',
    xtype : 'child_history_btn',
    config: {
        layout: {
            type: 'hbox',
            align : 'middle'
        },
        items: [
            {
                xtype : 'button', text : '뒤로', action:'HistoryCancel',flex : 0.5, margin:'20'
            }
        ]

    }

});