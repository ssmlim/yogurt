Ext.define('Yogurt.view.common.YogurtChild_TestSelect', {
    extend: 'Ext.Panel',
    xtype : 'yogurtchild_testselect',
    config: {
        layout: {
            type: 'vbox',
            align : 'middle'
        },
        defaults : {
            width : '300px',
            flex : 2.5
        },
        items: [
            {xtype: 'spacer'},
            {xtype : 'button', text : '일반검사', id:'NO', margin:'10px', cls : 'PinkTypeButton', iconCls:'compose'},
            {xtype : 'button', text : '정밀검사', id:'DE', margin:'10px', cls : 'PinkTypeButton', iconCls: 'search'},
            {xtype: 'spacer'}
        ]
    }
});