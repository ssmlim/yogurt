Ext.define('Yogurt.view.common.YogurtChild_ChildList', {
    extend: 'Ext.List',
    xtype : 'yogurtchild_childlist',
    config: {
        itemTpl: '[ {ChildName} ].  횟수 : {ReportCount}  </font></b>',
        store : {
            xclass : 'Yogurt.store.GetChildHistoryCountStore'
        },
        onItemDisclosure : true,
        striped: true
    }
});
