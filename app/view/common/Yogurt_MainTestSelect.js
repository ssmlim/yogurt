﻿Ext.define('Yogurt.view.common.Yogurt_MainTestSelect', {
    extend: 'Ext.Panel',
    xtype : 'yogurt_maintestselect',
    config: {
        layout: {
            type: 'hbox',
            align : 'middle'
        },
        items: [
            {xtype : 'button', text : '아동진단'+'<br>'+'(요구르트)', action:'ChildTest', flex: 1, margin:'20',cls : 'PinkTypeButton'},
            {xtype : 'button', text : '사회적응검사'+'<br>'+'(준비중)', action : 'TeacherTest', flex: 1, margin:'20',cls : 'PinkTypeButton'}
        ]
    }
});