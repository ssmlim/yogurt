Ext.define('Yogurt.view.common.Find_Password', {
    xtype: 'find_password',
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Email'
    ],
    config: {
        items: [
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '30%'
                },
                items: [
                    {
                        xtype         : 'emailfield',
                        id          : 'findId',
                        label         : '아이디',
                        autoCapitalize: true,
                        required      : true,
                        clearIcon     : true
                    },
                    {
                        xtype: 'textfield',
                        id: 'findName',
                        label : '이름',
                        required      : true

                    }
                ]
            },
            {
                html: '경고! 타인의 정보를 무단으로 사용시 법적책임이 따를 수 있습니다.',
                style : 'text-align:center',
                margin : '20px'
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'margin: .5em'
                },
                layout: {
                    type: 'vbox'
                },
                items: [
                    {type: 'button',text: '이메일로 임시 비밀번호 발송',margin: '10px', action: 'find_ok',cls : 'PinkTypeButton'},
                    {type: 'button',text: '취소',margin: '10px', action: 'find_cancel',cls : 'PinkTypeButton'}
                ]
            }
        ]
    }
});
