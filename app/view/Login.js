﻿Ext.define('Yogurt.view.Login', {
    extend: 'Ext.Container',
    xtype: 'login',
    requires : ['Ext.Img'],
    config: {
        style: 'background: #f993c2',
       
        layout: {
            type: 'vbox'
        },
        items: [
            { xtype : 'spacer' },
            {
                 xtype : 'image',
                 src: 'http://cyberco.cafe24.com/Yogurt/resources/icons/logo_white.png',
                 itemId :'logoBtn',
                 flex: 2,
                 height: '100%',
                 width: '100%'

            },
            { xtype : 'spacer'},
            { xtype : 'memberlogin', flex: 8 }
        ]
    }
});