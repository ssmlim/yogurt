
// ChildInfoController.js

Ext.define('Yogurt.controller.ChildInfoController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            Home : 'yogurt_main button[action=goHome]',
            Main: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },

            ChildInfo : { selector: 'childinfo', xtype: 'childinfo', autoCreate: true },
            ChildList: 'yogurt_main button[action=ChildList]',
            List: 'childinfo_list',

            ChildAdd: 'yogurt_main button[action=ChildAdd]',
            ChildAddOk: 'yogurt_main button[action=ChildAddOk]',

            ChildHistoryBtn : 'yogurt_main button[action=ChildHistory]',
            ChildEdit: 'yogurt_main button[action=ChildEdit]',
            ChildDelete: 'yogurt_main button[action=ChildDelete]',
            ChildCancel: 'yogurt_main button[action=ChildCancel]',

            ChildHistory : { selector: 'child_history', xtype: 'child_history', autoCreate: true },
            HistoryCancel: 'yogurt_main button[action=HistoryCancel]'


        },
        control: {
            ChildList: { tap: 'goChildList' },
            ChildAdd: { tap: 'goChildAdd' },
            Home : { tap : 'goHome'},

            List: { itemtap: 'goChilddetail' },

            ChildAddOk : { tap : 'onChildAddOk'},

            ChildHistoryBtn : { tap: 'goChildHistory' },
            ChildEdit : { tap: 'onChildEdit' },
            ChildDelete : { tap: 'onChildDelete' },
            ChildCancel : { tap : 'onChildback'},

            HistoryCancel : { tap : 'goChildInfo'}


        }
    },

    /*
     * refs: {
     main: 'mainpanel'
     },
     control: {
     main: {
     back: 'backButtonHandler'
     }
     }
     Then you will have your handler
     backButtonHandler: function(button){
     handler code goes here
     }
     * */


    goHome : function () {
        this.getMain().reset();
        this.getMain().getNavigationBar().query('button')[0].hide();
        this.getMain().getNavigationBar().query('button')[1].show();
        this.getMain().getNavigationBar().query('button')[2].show();
        this.getMain().getNavigationBar().query('button')[3].hide();
        this.getMain().getNavigationBar().query('button')[4].show();
    },

    goChildList: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");
        var getChildListStore = Ext.getStore('childliststore');
        getChildListStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });

        getChildListStore.load(function (records, operation, success) {
            if (success) {
                this.getMain().push({
                    xtype: 'childinfo'
                });
                this.getChildInfo().items.items[1].show();
                this.getChildInfo().items.items[1].setStore(getChildListStore);
                this.getMain().getNavigationBar().query('button')[0].show();
                this.getMain().getNavigationBar().query('button')[1].hide();
                this.getMain().getNavigationBar().query('button')[2].hide();
                this.getMain().getNavigationBar().query('button')[3].show();
                this.getMain().getNavigationBar().query('button')[4].hide();
            }
            else {
                this.getMain().push({
                    xtype: 'childinfo'
                });
                this.getChildInfo().items.items[2].show();
                this.getMain().getNavigationBar().query('button')[0].show();
                this.getMain().getNavigationBar().query('button')[1].hide();
                this.getMain().getNavigationBar().query('button')[2].hide();
                this.getMain().getNavigationBar().query('button')[3].show();
                this.getMain().getNavigationBar().query('button')[4].hide();
            }
        }, this);
    },

    goChildAdd : function () {
        this.getMain().push({
            xtype: 'childadd_form'
        });

        this.getMain().getNavigationBar().query('button')[3].hide();
    },

    onChildAddOk : function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        var ChildSex = '';
        var ChildName = Ext.getCmp('AChildName').getValue();
        var ChildBirth = Ext.getCmp('AChildBirth').getValue();
        if (Ext.getCmp('AChildSex1').isChecked()) {
            ChildSex = '여자'
        }
        else if (Ext.getCmp('AChildSex2').isChecked()){
            ChildSex = '남자'
        }

        var params = {
            'ChildName' : ChildName,
            'ChildBirth' : ChildBirth,
            'ChildSex': ChildSex,
            'Unum':CookieUnum
        };

        if (ChildName == '') {
            Ext.Msg.alert('알림', '이름을 입력해주세요', Ext.emptyFn);
        }
        else if(ChildBirth == '' ){
            Ext.Msg.alert('알림', '생년월일을 입력해주세요.', Ext.emptyFn);
        }
        else if((!Ext.getCmp('AChildSex1').isChecked())&&(!Ext.getCmp('AChildSex2').isChecked())){
            Ext.Msg.alert('알림', '성별을 선택해주세요.', Ext.emptyFn);
        }
        else {
            Ext.Ajax.request({
                url: 'AddChild.php',
                params: params,
                success: function (response) {
                    this.onChildback();
                    this.setChildList();
                    Ext.Msg.alert('알림', '추가 되었습니다.', Ext.emptyFn);
                }, scope: this
            });
        }
    },


    setChildList :function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        this.getChildInfo().items.items[1].hide();
        this.getChildInfo().items.items[2].hide();
        var getChildListStore = Ext.getStore('childliststore');
        getChildListStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });
        getChildListStore.load(function (records, operation, success) {
            if (success) {
                this.getChildInfo().items.items[1].show();
                this.getChildInfo().items.items[1].setStore(getChildListStore);
            }
            else {
                this.getChildInfo().items.items[2].show();
            }
        }, this);
    },

    // dataview (view, index, target, record, event)
    goChilddetail: function (list, index, target, record, e, eOpts) {
        this.getMain().push({
            xtype: 'childdetail_form'
        });

        if (record.get('ChildSex') == '여자') {
            Ext.getCmp('DChildSex1').check();
        }
        else if (record.get('ChildSex') == '남자') {
            Ext.getCmp('DChildSex2').check();
        }
        Ext.getCmp('DCnum').setValue(record.get('Cnum'));
        Ext.getCmp('DChildName').setValue(record.get('ChildName'));
        Ext.getCmp('DChildBirth').setValue(new Date(record.get('ChildBirth')));
        this.getMain().getNavigationBar().query('button')[3].hide();
    },

    onChildEdit: function () {

        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        var ChildSex = '';
        var ChildName = Ext.getCmp('DChildName').getValue();
        var ChildBirth = Ext.getCmp('DChildBirth').getValue();
        var Cnum = Ext.getCmp('DCnum').getValue();

        if (Ext.getCmp('DChildSex1').isChecked()) {
            ChildSex = '여자'
        }
        else if (Ext.getCmp('DChildSex2').isChecked()) {
            ChildSex = '남자'
        }

        var params = {
            'ChildName': ChildName,
            'ChildBirth': ChildBirth,
            'Cnum': Cnum,
            'ChildSex': ChildSex,
            'Unum': CookieUnum
        };

        if (ChildName == '') {
            Ext.Msg.alert('알림', '이름을 입력해주세요', Ext.emptyFn);
        }
        else if (ChildBirth == '') {
            Ext.Msg.alert('알림', '생년월일을 입력해주세요.', Ext.emptyFn);
        }

        else {
            Ext.Ajax.request({
                url: 'EditChild.php',
                params: params,
                success: function (response) {
                    console.log(response);
                    this.onChildback();
                    this.setChildList();
                    Ext.Msg.alert('알림', '수정 되었습니다.', Ext.emptyFn);
                }, scope: this
            });
        }
    },

    onChildDelete: function () {

        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        Ext.Msg.confirm("알림", "정말 삭제하시겠습니까?", function (button) {
            if (button == 'yes') {
                var Cnum = Ext.getCmp('DCnum').getValue();

                var params = {
                    'Cnum': Cnum,
                    'Unum': CookieUnum
                };

                Ext.Ajax.request({
                    url: 'DeleteChild.php',
                    params: params,
                    success: function (response) {
                        this.onChildback();
                        this.setChildList();
                        Ext.Msg.alert('알림', '삭제 되었습니다.', Ext.emptyFn);
                    }, scope: this });
            }
            else {
                return false;
            }
        }, this );
    },

    onChildback: function () {
        this.getMain().pop();
        this.getMain().getNavigationBar().query('button')[3].show();
    },


    goChildHistory: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");
        var Cnum = Ext.getCmp('DCnum').getValue();

        var getChildHistoryStore = Ext.getStore('getchildhistorystore');
        getChildHistoryStore.getProxy().setExtraParams({
            'Unum': CookieUnum,
            'Cnum': Cnum
        });

        getChildHistoryStore.load(function (records, operation, success) {
            if (success) {
                this.getMain().push({
                    xtype: 'child_history'
                });
                this.getChildHistory().items.items[0].setStore(getChildHistoryStore);
            }
            else {
                Ext.Msg.alert('알림', '검사 이력이 없습니다.', Ext.emptyFn);
            }
        }, this);


    },
    goChildInfo : function () {
        this.getMain().pop();
        this.getMain().getNavigationBar().query('button')[3].hide();
    }

});