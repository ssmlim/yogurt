Ext.define('Yogurt.controller.FindController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            FindOk_btn: 'find button[action=find_ok]',
            FindCancel_btn: 'find button[action=find_cancel]',
            Login: { selector: 'login', xtype: 'login', autoCreate: true }
        },
        control: {
            FindOk_btn: { tap: 'goFind' },
            FindCancel_btn: { tap: 'goCancel' }
        }
    },

    encryptionMd5 : function (pwd) {
        var me = this;
        var md5 = me.getApplication().getController('md5_controller');
        return md5.hex_md5(pwd);
    },

    goFind: function () {
        var joinOrFind = 'Find';      // true : 가입할때 이메일 체크 , false : 비번찾을 때 체크
        var findName = Ext.getCmp('findName').getValue();
        var findId = Ext.getCmp('findId').getValue();

        var rndpwd	= String(Math.floor(Math.random() * (99999999 - 10000000 + 1)) + 10000000);
        var md_pass = this.encryptionMd5(rndpwd);

        var params = {
            'findId': findId,
            'findName': findName,
            'joinOrFind': joinOrFind,
            'newPwd' : rndpwd,
            'md5Pwd' : md_pass
        };

        if (findId == '') {
            Ext.Msg.alert('알림', '가입한 메일을 입력해주세요', Ext.emptyFn);
        }
        else if (findName == '') {
            Ext.Msg.alert('알림', '가입한 이름을 입력해주세요.', Ext.emptyFn);
        }
        else {
            Ext.Ajax.request({
                url: 'CheckEmail.php',
                params: params,
                success: function (response) {
                    var text = response.responseText;
                    if (text === 'UidNo') {
                        Ext.Msg.alert('알림', '존재하는 아이디입니다.', Ext.emptyFn);
                    }
                    else if (text == 'UnameNo') {
                        Ext.Msg.alert('알림', '정보가 일치하지 않습니다.', Ext.emptyFn);
                    }
                    else if (text === 'UidUnameOk') {
                        Ext.Ajax.request({
                            url: 'SendFindMail.php',
                            params: params,
                            success: function (response) {
                                var text = response.responseText;
                                Ext.Msg.alert('알림', '임시 비밀번호가 ' + findId + '으로 전송되었습니다.', Ext.emptyFn);
                                this.moveLoginpage();
                            },scope : this
                        });
                    }
                }, scope : this
            });
        }
    },

    moveLoginpage : function () {
        Ext.getCmp('findId').setValue('');
        Ext.getCmp('findName').setValue('');
    Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'down' });
    Ext.Viewport.setActiveItem(this.getLogin());
},

    goCancel: function () {
        Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'down' });
        Ext.Viewport.setActiveItem(this.getLogin());

        Ext.getCmp('loginID').reset();
        Ext.getCmp('loginPWD').reset();
    }




});