var getYogurtChildHistoryStore = '';
var Cname = '';
var Cnum = 0;
Ext.define('Yogurt.controller.YogurtChild_MainController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            Main: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },

            ChildTest_btn: 'yogurt_main button[action=ChildTest]',
            TeacherTest_btn: 'yogurt_main button[action=TeacherTest]',

            ChildTestList: 'yogurtchild_testchildlist',

            ChildTest: { selector: 'yogurtchild_test', xtype: 'yogurtchild_test', autoCreate: true },
            YChildMain: {selector: 'yogurtchild_main', xtype: 'yogurtchild_main', autoCreate: true}

        },
        control: {
            ChildTest_btn: { tap: 'goChildSelect' },
            TeacherTest_btn: { tap: 'goTeacherTest'},
            ChildTestList: { itemtap: 'goChildTest'}
        }
    },

    goChildSelect: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");
        getYogurtChildHistoryStore = Ext.getStore('getchildhistorycountstore');
        getYogurtChildHistoryStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });

        getYogurtChildHistoryStore.load(function (records, operation, success) {
            if (success) {

                this.getMain().push({
                    xtype: 'yogurtchild_test'
                });
                this.SetTestPageMenu();
                this.getChildTest().items.items[1].setStore(getYogurtChildHistoryStore);
            }
            else {
                Ext.Msg.alert('알림', '아동정보가 없습니다.', Ext.emptyFn);
                var ChildInfoController = me.getApplication().getController('ChildInfoController');
                ChildInfoController.goChildList();
            }
        }, this);
    },

    SetTestPageMenu: function () {
        this.getMain().getNavigationBar().query('button')[0].show();
        this.getMain().getNavigationBar().query('button')[1].hide();
        this.getMain().getNavigationBar().query('button')[2].hide();
        this.getMain().getNavigationBar().query('button')[3].hide();
        this.getMain().getNavigationBar().query('button')[4].hide();

    },

    goTeacherTest: function () {
        Ext.Msg.alert('알림', '준비 중입니다.');
    },


    goChildTest: function (list, index, target, record, e, eOpts) {
        Cnum = record.get('Cnum');
        Cname = record.get('ChildName');
        this.setChildTestInfo(Cnum, Cname);
        this.getMain().push({
            xtype: 'yogurtchild_main',
            title : Cname
        });
        this.getYChildMain().getNavigationBar().titleComponent.setTitle(Cname);
    },

    setChildTestInfo: function (getCnum, getCname) {
        Cnum = getCnum;
        Cname = getCname;
    },
    getChildTestInfo: function () {
        return Cnum;
    }

});