var questionStore = 0;
var Qnum = 0;
var questionParams = '';
var NonCompleteQnumPos = 0;

Ext.define('Yogurt.controller.YogurtChild_TestController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            YChildMain: {selector: 'yogurtchild_main', xtype: 'yogurtchild_main', autoCreate: true},
            Main: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },

            NormalTest: 'yogurtchild_testselect #NO',
            DetailTest: 'yogurtchild_testselect #DE',

            // 이전 문항, 다음 문항 이동버튼
            BackBtn: 'yogurtchild_main button[action=back]',
            ForwardBtn: 'yogurtchild_main button[action=forward]',

            // 검사점수버튼
            AnswerBtn1: 'yogurtchild_testmain #goodresult',
            AnswerBtn2: 'yogurtchild_testmain #normalresult',
            AnswerBtn3: 'yogurtchild_testmain #badresult',
            CkNonAnswer : 'yogurtchild_main button[action=NonAnswer]',
            ExitTest : 'yogurtchild_main button[action=ExitTest]',

            NonAnswerList: 'yogurtchild_nonanswerlist',

            YChildMainFrame: { selector: 'yogurtchild_testmain', xtype: 'yogurtchild_testmain', autoCreate: true },
            YChildNonAnswer: { selector: 'yogurtchild_nonanswer', xtype: 'yogurtchild_nonanswer', autoCreate: true },
            YChildTestResult: { selector: 'yogurtchild_result', xtype: 'yogurtchild_result', autoCreate: true },

            ShowTestGraph : 'yogurtchild_main button[action=TestResultGraph]',
            ShowTestResult : 'yogurtchild_main button[action=TestResultList]',
            ResultSendMail : 'yogurtchild_main button[action=ResultSendMail]'
        },
        control: {
            NormalTest: { tap: 'goTest'},
            DetailTest: { tap: 'goTest'},

            BackBtn: { tap: 'goBack' },
            ForwardBtn: { tap: 'goForward' },

            AnswerBtn1: { tap: 'onSelectBtn' },
            AnswerBtn2: { tap: 'onSelectBtn' },
            AnswerBtn3: { tap: 'onSelectBtn' },
            CkNonAnswer : { tap: 'goNonAnswerList' },
            ExitTest : { tap: 'goExitTest' },
            NonAnswerList: {itemtap: 'onSelectNonAnswer'},

            ShowTestGraph : { tap : 'ShowResultGraph'},
            ShowTestResult : {tap : 'ShowResultList'},
            ResultSendMail : {tap : 'SendResultMail'}
        }
    },

    setquestionParams: function (Cnum, LoginUnum, Anum, Qtype, qCount, flag, NonCompleteQnum) {

        if (flag == 'on') {
            questionParams = {
                Cnum: Cnum,
                Unum: LoginUnum,
                Anum: Anum,
                Qtype: Qtype,
                qCount: qCount,
                flag: flag,
                NonCompleteQnum: NonCompleteQnum
            };
            NonCompleteQnumPos = 0;
        }
        else if (flag == 'off') {
            questionParams = {
                Cnum: Cnum,
                Unum: LoginUnum,
                Anum: Anum,
                Qtype: Qtype,
                qCount: qCount,
                flag: flag
            };
        }
    },

    getquestionParams: function () {
        return questionParams
    },

    goTest: function (resultBtnObj) {
        var testCode = resultBtnObj.id;

        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var yc_mainctrl = me.getApplication().getController('YogurtChild_MainController');

        var LoginUnum = cookie_ctrl.readCookie('LoginUnum');
        var Cnum = yc_mainctrl.getChildTestInfo();
        var getPreReportStore = Ext.getStore('getprereportstore');

        getPreReportStore.getProxy().setExtraParams({
            'Cnum': Cnum,
            'Unum': LoginUnum,
            'Qtype': testCode
        });

        getPreReportStore.load({callback: function (records, operation, success) {
            var result = getPreReportStore.getData().items[0].data.result;
            var Qtype = getPreReportStore.getData().items[0].data.Qtype;
            var qCount = parseInt(getPreReportStore.getData().items[0].data.questionCount);
            if (success) {
                var curAnum = parseInt(getPreReportStore.getData().items[0].data.curAnum);
                if (result == 'NoReport' || result == 'NoNonCompleteAnum') {
                    this.setquestionParams(Cnum, LoginUnum, curAnum, Qtype, qCount, 'off', '');
                    Qnum = 1;
                    this.setQuestion(Qnum, questionParams.Qtype);

                }
                else if (result == 'NonComplete') {
                    var NonCompleteAnum = parseInt(getPreReportStore.getData().items[0].data.NonCompleteAnum);
                    var NonCompleteQnum = getPreReportStore.getData().items[0].data.NonCompleteQnum;
                    this.setquestionParams(Cnum, LoginUnum, NonCompleteAnum, Qtype, qCount, 'on', NonCompleteQnum);


                    Ext.Msg.confirm("알림", "진행 중인 테스트가 있습니다.<br> 계속하시겠습니까?", function (button) {
                        if (button == 'yes') {
                            this.setQuestion(NonCompleteQnum[NonCompleteQnumPos],questionParams.Qtype);
                        }
                        else {
                            Ext.Msg.confirm("알림", "새로 진행 하시겠습니까?", function (button) {
                                if (button == 'yes') {
                                    var setDeleteParams = {
                                        'Unum': LoginUnum,
                                        'Cnum': Cnum,
                                        'Anum': questionParams.Anum
                                    };

                                    Ext.Ajax.request({
                                        url: 'setDeleteAnum.php',
                                        params: setDeleteParams,
                                        success: function (response) {
                                        }, scope: this
                                    });
                                }
                                else {
                                    return false;
                                }
                            }, this);
                        }
                    }, this);
                }
            }
        }, scope: this
        });
    },

    goBack: function () {
        if (questionParams.flag == 'off') {
            Qnum = parseInt(questionStore.getData().items[0].data.Qnum);
            Qnum = Qnum - 1;
            if (Qnum == 0) {
                this.getYChildMain().pop();
                this.getYChildMain().getNavigationBar().query('button')[0].hide();
                this.getYChildMain().getNavigationBar().query('button')[1].hide();
            }
            else {
                questionStore.getProxy().setExtraParams({"Qnum": Qnum, Qtype : questionParams.Qtype});
                questionStore.load();
            }
        }
        else {
            if (NonCompleteQnumPos == 0) {
                this.getYChildMain().pop();
                this.getYChildMain().getNavigationBar().query('button')[0].hide();
                this.getYChildMain().getNavigationBar().query('button')[1].hide();
            }
            else {
                NonCompleteQnumPos = NonCompleteQnumPos - 1;
                Qnum = questionParams.NonCompleteQnum[NonCompleteQnumPos];
                questionStore.getProxy().setExtraParams({"Qnum": Qnum,Qtype :questionParams.Qtype});
                questionStore.load();
            }

        }

    },

    goForward: function () {
        if (questionParams.flag == 'off') {
            Qnum = parseInt(questionStore.getData().items[0].data.Qnum);
            if (Qnum < questionParams.qCount) {
                Qnum = Qnum + 1;
                questionStore.getProxy().setExtraParams({"Qnum": Qnum,Qtype :questionParams.Qtype});
                questionStore.load();
            }
            else {
                this.goNonAnswerList();
            }
        }
        else {
            if (NonCompleteQnumPos < questionParams.NonCompleteQnum.length - 1) {
                NonCompleteQnumPos = NonCompleteQnumPos + 1;
                Qnum = questionParams.NonCompleteQnum[NonCompleteQnumPos];
                questionStore.getProxy().setExtraParams({"Qnum": Qnum,Qtype :questionParams.Qtype});
                questionStore.load();
            }
            else {
                this.goNonAnswerList();
            }
        }
    },

    setQuestion: function (getQnum, getQtype) {
        Qnum = getQnum;
        questionStore = Ext.getStore('questionstoreId');
        questionStore.getProxy().setExtraParams({"Qnum": Qnum, Qtype : getQtype});
        questionStore.load();
        this.getYChildMain().push({
            xtype: 'yogurtchild_testmain'
        });
        this.getYChildMain().getNavigationBar().query('button')[0].show();
        this.getYChildMain().getNavigationBar().query('button')[1].show();
        this.getYChildMainFrame().items.items[0].setStore(questionStore);
    },

    onSelectBtn: function (resultBtnObj) {
        var Aanswer = 0;
        switch (resultBtnObj.id) {
            case 'answerGood' :
                Aanswer = 5;
                break;
            case 'answerNormal' :
                Aanswer = 3;
                break;
            case 'answerBad' :
                Aanswer = 1;
                break;
        }
        var setAnswerParams = {
            'Unum': questionParams.Unum,
            'Cnum': questionParams.Cnum,
            'Qtype': questionParams.Qtype,
            'Anum': questionParams.Anum,
            'Qnum': Qnum,
            'Aanswer': Aanswer
        };

        Ext.Ajax.request({
            url: 'setAnswer.php',
            params: setAnswerParams,
            success: function (response) {
                this.goForward();
            }, scope: this
        });
    },

    goExitTest : function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        this.getMain().pop();

        getYogurtChildHistoryStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });
        getYogurtChildHistoryStore.load()

    },

    goNonAnswerList: function () {
        this.getYChildMain().getNavigationBar().query('button')[0].hide();
        this.getYChildMain().getNavigationBar().query('button')[1].hide();
        var nonAnswerStore = Ext.getStore('getnonanswerstore');
        nonAnswerStore.getProxy().setExtraParams({
            'Anum': questionParams.Anum,
            'Unum': questionParams.Unum,
            'Cnum': questionParams.Cnum
        });
        nonAnswerStore.load(function (records, operation, success) {
            var getParam = this.getquestionParams();
            var NonCompleteQnum = [];
            for (var i = 0; i < nonAnswerStore.getData().items.length; i++) {

                NonCompleteQnum[i] = nonAnswerStore.getData().items[i].data.Qnum;
            }
            if (success) {

                this.setquestionParams(getParam.Cnum, getParam.Unum, getParam.Anum, getParam.Qtype, getParam.qCount, 'on', NonCompleteQnum);
                this.getYChildMain().push({
                    xtype: 'yogurtchild_nonanswer'
                });
                this.getYChildNonAnswer().items.items[0].setStore(nonAnswerStore);
            }
            else {
                this.goResult();
            }
        }, this);


    },

    onSelectNonAnswer: function (list, index, target, record, e, eOpts) {
        Qnum = record.get('Qnum');
        questionStore.getProxy().setExtraParams({Qnum: Qnum, Qtype : questionParams.Qtype});
        questionStore.load();

        this.getYChildMain().pop();
        this.getYChildMainFrame().items.items[0].setStore(questionStore);
    },

    goResult : function () {
        this.getYChildMain().push({
            xtype: 'yogurtchild_result'
        });

        this.ShowResultGraph();

    },

    ShowResultGraph : function () {
        var setiframe = "<iframe src='./app/kendo/KendoMain.php?Cnum=" + questionParams.Cnum +
            "&Unum="+questionParams.Unum+"&Anum="+questionParams.Anum+"&Qtype="+questionParams.Qtype+
            "' width='100%' height='100%' frameborder='0'></iframe>"
        this.getYChildTestResult().items.items[1].hide();
        this.getYChildTestResult().items.items[2].show();
        this.getYChildTestResult().items.items[2].setHtml(setiframe);
    },

    ShowResultList : function () {
        var getAnswerStore = Ext.getStore('getanswerstore');
        getAnswerStore.getProxy().setExtraParams({
            'Unum': questionParams.Unum,
            'Cnum': questionParams.Cnum,
            'Anum': questionParams.Anum,
            'Qtype' : questionParams.Qtype
        });

        getAnswerStore.load(function (records, operation, success) {
            if (success) {
                getAnswerStore.sort('Aid', 'ASC');
                this.getYChildTestResult().items.items[2].hide();
                this.getYChildTestResult().items.items[1].show();
                this.getYChildTestResult().items.items[1].setStore(getAnswerStore);
            }
        }, this);

    },

    SendResultMail : function() {

        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var LoginId = cookie_ctrl.readCookie("LoginId");
        var param = {
            'Cnum' :  questionParams.Cnum,
            'Unum' : questionParams.Unum,
            'Anum' :  questionParams.Anum,
            'Qtype' : questionParams.Qtype
        };

        Ext.Msg.confirm("알림", "가입한 메일주소("+LoginId+")로 <br>결과를 보내시겠습니까?", function (button) {
            if (button == 'yes') {
                Ext.Ajax.request({
                    url: 'ResultSendMail.php',
                    params: param,
                    success: function (response) {
                        Ext.Msg.alert('알림',LoginId+'로 메일을 전송하였습니다.',Ext.emptyFn);
                    }, scope: this });
            }
            else {
                return false
            }
        }, this );

    }
});