Ext.define('Yogurt.controller.JoinController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            JoinOk_Btn : 'join button[action=join_ok]',
            JoinCancel_Btn: 'join button[action=join_cancel]',
            Login: { selector: 'login', xtype: 'login', autoCreate: true }
        },
        control: {
            JoinOk_Btn: { tap : 'onJoinOk' },
            JoinCancel_Btn: { tap : 'onJoinCancel' }
        }
    },

    encryptionMd5 : function (pwd) {
        var me = this;
        var md5 = me.getApplication().getController('md5_controller');
        return md5.hex_md5(pwd);
    },

    onJoinOk: function () {
        var joinId = Ext.getCmp('joinId').getValue();
        var joinOrFind= 'Join';

        var params = {
            'joinId': joinId,
            'joinOrFind' : joinOrFind
        };



        Ext.Ajax.request({
            url: 'CheckEmail.php',
            params: params,
            success: function (response) {
                    var text = response.responseText;
                    if (text === 'UidNo') {
                        Ext.Msg.alert('알림', '존재하는 아이디입니다. 다른 아이디를 입력하세요', Ext.emptyFn);
                    }
                    else if (text === 'UidOk') {
                        var joinName = Ext.getCmp('joinName').getValue();
                        var joinPassword = Ext.getCmp('joinPassword').getValue();
                        var joinPasswordcheck = Ext.getCmp('joinPasswordcheck').getValue();
                        var md5_pass = this.encryptionMd5(joinPassword);
                        var joinPhone = Ext.getCmp('joinPhone').getValue();
                        var joinCheck = Ext.getCmp('joinCheck')._checked;
                        var params = {
                            'joinId': joinId,
                            'joinName': joinName,
                            'joinPassword': md5_pass,
                            'joinPhone': joinPhone
                        };

                        if (joinCheck == false) {
                            Ext.Msg.alert('알림', '정보 제공에 동의해주십시오.', Ext.emptyFn);
                        }
                        else if(joinId == '' ){
                            Ext.Msg.alert('알림', '이메일을 입력해주십시오.', Ext.emptyFn);
                        }
                        else if (joinName == '') {
                            Ext.Msg.alert('알림', '이름을 입력해주십시오.', Ext.emptyFn);
                        }
                        else if (joinPassword == '' || joinPasswordcheck == '') {
                            Ext.Msg.alert('알림', '비밀번호를 입력해주십시오.', Ext.emptyFn);
                        }
                        else if (joinPhone == '') {
                            Ext.Msg.alert('알림', '휴대폰번호를 입력해주십시오.', Ext.emptyFn);
                        }
                        else if (joinPassword != joinPasswordcheck){
                            Ext.Msg.alert('알림', '비밀번호가 맞지 않습니다.', Ext.emptyFn);
                        }

                        else {
                            var emailCheck = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

                            if (joinId.length < 6 || !emailCheck.test(joinId)) {
                                Ext.Msg.alert('알림',"메일 형식이 맞지 않습니다.");
                            }
                            else if (joinPassword.length < 8 || joinPasswordcheck.length < 8) {
                                Ext.Msg.alert('알림', "비밀번호는 8자리 이상으로 해야합니다.");
                            }
                            else if (!Number(joinPhone)) {
                                Ext.Msg.alert('알림', "휴대폰번호는 숫자만 가능합니다.");
                            }

                            else {
                                Ext.Ajax.request({
                                    url: 'Join.php',
                                    params: params,
                                    success: function (response) {
                                        var text = response.responseText;

                                        Ext.getCmp('joinId').setValue('');
                                        Ext.getCmp('joinName').setValue('');
                                        Ext.getCmp('joinPassword').setValue('');
                                        Ext.getCmp('joinPasswordcheck').setValue('');
                                        Ext.getCmp('joinPhone').setValue('');
                                        Ext.getCmp('joinCheck').reset();

                                        Ext.Msg.alert('알림', '가입이 완료되었습니다.', Ext.emptyFn);
                                        Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'down' });
                                        Ext.Viewport.setActiveItem(this.getLogin());
                                    }, scope :this
                                });
                            }
                        }
                    }
                }
            , scope: this
        });


    },

    onJoinCancel: function () {
        Ext.Msg.confirm("알림", "가입을 취소하시겠습니까?", function(button) {
            if(button == "yes"){
                Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'down' });
                Ext.Viewport.setActiveItem(this.getLogin());
            }
            else {
                return false
            }
        }, this );
    }
});