var param='';
Ext.define('Yogurt.controller.ChildInfoController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            Home : 'yogurt_main button[action=goHome]',
            Main: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },

            ChildInfo : { selector: 'childinfo', xtype: 'childinfo', autoCreate: true },
            ChildList: 'yogurt_main button[action=ChildList]',
            List: 'childinfo_list',
            HistoryList : 'child_history_list',
            ChildAdd: 'yogurt_main button[action=ChildAdd]',
            ChildAddOk: 'yogurt_main button[action=ChildAddOk]',

            ChildHistoryBtn : 'yogurt_main button[action=ChildHistory]',
            ChildEdit: 'yogurt_main button[action=ChildEdit]',
            ChildDelete: 'yogurt_main #ChildDelete',
            ChildCancel: 'yogurt_main button[action=ChildCancel]',

            ChildHistory : { selector: 'child_history', xtype: 'child_history', autoCreate: true },
            ChildHistoryChart : { selector: 'yogurtchild_chart', xtype: 'yogurtchild_chart', autoCreate: true },
            HistoryCancel: 'yogurt_main button[action=HistoryCancel]',
            ChartCancel : 'yogurt_main button[action=ChartCancel]',

            ShowGraph : 'yogurt_main button[action=Graph]',
            ShowResult : 'yogurt_main button[action=ResultList]',
            SendResultMail :'yogurt_main button[action=Mail]',

            NormalTest: 'child_history #NO',
            DetailTest: 'child_history #DE'

        },
        control: {
            ChildList: { tap: 'goChildList' },
            ChildAdd: { tap: 'goChildAdd' },
            Home : { tap : 'goHome'},

            List: { itemtap: 'getListTarget' },
            HistoryList : { itemtap : 'goChildHistoryChart'},

            ChildAddOk : { tap : 'onChildAddOk'},

            ChildHistoryBtn : { tap: 'goChildHistory' },
            ChildEdit : { tap: 'onChildEdit' },
            ChildDelete : { tap: 'onChildDelete' },
            ChildCancel : { tap : 'onChildback'},

            HistoryCancel : { tap : 'onChildback'},
            ChartCancel : {tap : 'onback'},

            ShowGraph : { tap : 'ShowResultGraph'},
            ShowResult : {tap : 'ShowResultList'},
            SendResultMail : {tap: 'SendResultMail'},

            NormalTest: { tap: 'goTotalGraph'},
            DetailTest: { tap: 'goTotalGraph'}

        }
    },

    onback : function () {
        this.getMain().pop();
    },

    goHome : function () {
        this.getMain().reset();
        this.getMain().getNavigationBar().query('button')[0].hide();
        this.getMain().getNavigationBar().query('button')[1].show();
        this.getMain().getNavigationBar().query('button')[2].show();
        this.getMain().getNavigationBar().query('button')[3].hide();
        this.getMain().getNavigationBar().query('button')[4].show();
    },

    goChildList: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");
        var getChildListStore = Ext.getStore('childliststore');
        getChildListStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });

        getChildListStore.load(function (records, operation, success) {
            if (success) {
                this.getMain().push({
                    xtype: 'childinfo'
                });
                this.getChildInfo().items.items[1].show();
                this.getChildInfo().items.items[1].setStore(getChildListStore);
                this.getMain().getNavigationBar().query('button')[0].show();
                this.getMain().getNavigationBar().query('button')[1].hide();
                this.getMain().getNavigationBar().query('button')[2].hide();
                this.getMain().getNavigationBar().query('button')[3].show();
                this.getMain().getNavigationBar().query('button')[4].hide();
            }
            else {
                this.getMain().push({
                    xtype: 'childinfo'
                });
                this.getChildInfo().items.items[2].show();
                this.getMain().getNavigationBar().query('button')[0].show();
                this.getMain().getNavigationBar().query('button')[1].hide();
                this.getMain().getNavigationBar().query('button')[2].hide();
                this.getMain().getNavigationBar().query('button')[3].show();
                this.getMain().getNavigationBar().query('button')[4].hide();
            }
        }, this);
    },

    goChildAdd : function () {
        this.getMain().push({
            xtype: 'childadd_form'
        });

        this.getMain().getNavigationBar().query('button')[3].hide();
    },

    onChildAddOk : function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        var ChildSex = '';
        var ChildName = Ext.getCmp('AChildName').getValue();
        var ChildBirth = Ext.getCmp('AChildBirth').getValue();
        if (Ext.getCmp('AChildSex1').isChecked()) {
            ChildSex = '여자'
        }
        else if (Ext.getCmp('AChildSex2').isChecked()){
            ChildSex = '남자'
        }

        var params = {
            'ChildName' : ChildName,
            'ChildBirth' : ChildBirth,
            'ChildSex': ChildSex,
            'Unum':CookieUnum
        };

        if (ChildName == '') {
            Ext.Msg.alert('알림', '이름을 입력해주세요', Ext.emptyFn);
        }
        else if(ChildBirth == '' ){
            Ext.Msg.alert('알림', '생년월일을 입력해주세요.', Ext.emptyFn);
        }
        else if((!Ext.getCmp('AChildSex1').isChecked())&&(!Ext.getCmp('AChildSex2').isChecked())){
            Ext.Msg.alert('알림', '성별을 선택해주세요.', Ext.emptyFn);
        }
        else {
            Ext.Ajax.request({
                url: 'AddChild.php',
                params: params,
                success: function (response) {
                    this.onChildback();
                    this.setChildList();
                    Ext.Msg.alert('알림', '추가 되었습니다.', Ext.emptyFn);
                }, scope: this
            });
        }
    },


    setChildList :function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        this.getChildInfo().items.items[1].hide();
        this.getChildInfo().items.items[2].hide();
        var getChildListStore = Ext.getStore('childliststore');
        getChildListStore.getProxy().setExtraParams({
            'Unum': CookieUnum
        });
        getChildListStore.load(function (records, operation, success) {
            if (success) {
                this.getChildInfo().items.items[1].show();
                this.getChildInfo().items.items[1].setStore(getChildListStore);
            }
            else {
                this.getChildInfo().items.items[2].show();
            }
        }, this);
    },


    getListTarget : function (list, index, target, record, e, eOpts) {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");
        var Cnum = record.get('Cnum');
        var ChildName = record.get('ChildName');
        var ChildBirth = record.get('ChildBirth');
        var ChildSex = record.get('ChildSex');

        param = {
            'Cnum': Cnum,
            'Unum': CookieUnum,
            'Cname' : ChildName
        };


        if (e.getTarget('span.ChildEdit')) {
            this.goChilddetail(Cnum, ChildName, ChildBirth, ChildSex);
        }
        else if (e.getTarget('span.ChildDelete')) {
            this.onChildDelete(CookieUnum, Cnum);
        }
        else if (e.getTarget('span.ChildHistory')) {
            this.goChildHistory(CookieUnum, Cnum, ChildName);
        }
        e.stopEvent();
    },

    goChilddetail: function (Cnum, ChildName, ChildBirth, ChildSex) {
            this.getMain().push({
                xtype: 'childdetail_form'
            });

            if (ChildSex == '여자') {
                Ext.getCmp('DChildSex1').check();
            }
            else if (ChildSex == '남자') {
                Ext.getCmp('DChildSex2').check();
            }
            Ext.getCmp('DCnum').setValue(Cnum);
            Ext.getCmp('DChildName').setValue(ChildName);
            Ext.getCmp('DChildBirth').setValue(new Date(ChildBirth));
            this.getMain().getNavigationBar().query('button')[3].hide();
    },

    onChildEdit: function () {

        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var CookieUnum = cookie_ctrl.readCookie("LoginUnum");

        var ChildSex = '';
        var ChildName = Ext.getCmp('DChildName').getValue();
        var ChildBirth = Ext.getCmp('DChildBirth').getValue();
        var Cnum = Ext.getCmp('DCnum').getValue();

        if (Ext.getCmp('DChildSex1').isChecked()) {
            ChildSex = '여자'
        }
        else if (Ext.getCmp('DChildSex2').isChecked()) {
            ChildSex = '남자'
        }

        var params = {
            'ChildName': ChildName,
            'ChildBirth': ChildBirth,
            'Cnum': Cnum,
            'ChildSex': ChildSex,
            'Unum': CookieUnum
        };

        if (ChildName == '') {
            Ext.Msg.alert('알림', '이름을 입력해주세요', Ext.emptyFn);
        }
        else if (ChildBirth == '') {
            Ext.Msg.alert('알림', '생년월일을 입력해주세요.', Ext.emptyFn);
        }

        else {
            Ext.Ajax.request({
                url: 'EditChild.php',
                params: params,
                success: function (response) {
                    console.log(response);
                    this.onChildback();
                    this.setChildList();
                    Ext.Msg.alert('알림', '수정 되었습니다.', Ext.emptyFn);
                }, scope: this
            });
        }
    },

    onChildDelete: function (Unum, Cnum) {
        Ext.Msg.confirm("알림", "정말 삭제하시겠습니까?<br> 검사 결과 등의 관련된 모든 항목이 삭제 됩니다.", function (button) {
            if (button == 'yes') {
                var params = {
                    'Cnum': Cnum,
                    'Unum': Unum
                };

                Ext.Ajax.request({
                    url: 'DeleteChild.php',
                    params: params,
                    success: function (response) {
                        this.setChildList();
                        Ext.Msg.alert('알림', '삭제 되었습니다.', Ext.emptyFn);
                    }, scope: this });
            }
            else {
                return false;
            }
        }, this );
    },

    onChildback: function () {
        this.getMain().pop();
        this.getMain().getNavigationBar().query('button')[3].show();
    },


    goChildHistory: function (Unum, Cnum, Cname) {
        var getChildHistoryStore = Ext.getStore('getchildhistorystore');
        getChildHistoryStore.getProxy().setExtraParams({
            'Unum': Unum,
            'Cnum': Cnum
        });

        getChildHistoryStore.load(function (records, operation, success) {
            if (success) {
                this.getMain().push({
                    xtype: 'child_history'
                });
                this.getChildHistory().items.items[0].setStore(getChildHistoryStore);
                this.getMain().getNavigationBar().query('button')[3].hide();
                this.getChildHistory().items.items[1].setTitle(Cname+'의 검사기록');
            }
            else {
                Ext.Msg.alert('알림', '검사 이력이 없습니다.', Ext.emptyFn);
            }
        }, this);
    },

    goTotalGraph : function (resultBtnObj) {

        this.getMain().push({
            xtype: 'yogurtchild_chart'
        });
        var Qtype = resultBtnObj.id;

        var setiframe = "<iframe src='./app/kendo/KendoTotalMain.php?Cnum=" + param.Cnum +
            "&Unum="+param.Unum+"&Qtype="+Qtype+"' width='100%' height='100%' frameborder='0'></iframe>"
        this.getChildHistoryChart().items.items[0].setTitle(param.Cname+'의 검사결과');
        this.getChildHistoryChart().items.items[1].hide();
        this.getChildHistoryChart().items.items[2].hide();
        this.getChildHistoryChart().items.items[3].setHtml(setiframe);
    },

     goChildHistoryChart : function (list, index, target, record, e, eOpts) {
         param = {
            'Cnum' :  record.get('Cnum'),
            'Unum' : record.get('Unum'),
            'Anum' :  record.get('Anum'),
            'Qtype' : record.get('Qtype')
         };

         this.getMain().push({
             xtype: 'yogurtchild_chart'
         });

         this.getChildHistoryChart().items.items[0].setTitle(record.get('ChildName')+'의 검사결과');
         this.ShowResultGraph();
     },

    ShowResultGraph : function () {
        var setiframe = "<iframe src='./app/kendo/KendoMain.php?Cnum=" + param.Cnum +
            "&Unum="+param.Unum+"&Anum="+param.Anum+"&Qtype="+param.Qtype+
            "' width='100%' height='100%' frameborder='0'></iframe>"
        this.getChildHistoryChart().items.items[2].hide();
        this.getChildHistoryChart().items.items[3].show();
        this.getChildHistoryChart().items.items[3].setHtml(setiframe);
    },


    ShowResultList : function () {
        var getAnswerStore = Ext.getStore('getanswerstore');
        getAnswerStore.getProxy().setExtraParams({
            'Unum': param.Unum,
            'Cnum': param.Cnum,
            'Anum': param.Anum,
            'Qtype' : param.Qtype
        });

        getAnswerStore.load(function (records, operation, success) {
            if (success) {
                getAnswerStore.sort('Aid', 'ASC');
                this.getChildHistoryChart().items.items[3].hide();
                this.getChildHistoryChart().items.items[2].show();
                this.getChildHistoryChart().items.items[2].setStore(getAnswerStore);
            }
        }, this);

    },

    SendResultMail : function() {

         var me = this;
         var cookie_ctrl = me.getApplication().getController('Cookie');
         var LoginId = cookie_ctrl.readCookie("LoginId");

         Ext.Msg.confirm("알림", "가입한 메일주소("+LoginId+")로 <br>결과를 보내시겠습니까?", function (button) {
         if (button == 'yes') {
         Ext.Ajax.request({
         url: 'ResultSendMail.php',
         params: param,
         success: function (response) {
         Ext.Msg.alert('알림',LoginId+'로 메일을 전송하였습니다.',Ext.emptyFn);
         }, scope: this });
         }
         else {
         return false
         }
         }, this );

    }
});