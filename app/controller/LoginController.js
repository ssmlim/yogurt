Ext.define('Yogurt.controller.LoginController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            LoginBtn : 'login button[action=login]',

            YogurtMain: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },

            FindpwBtn: 'login button[action=findpw]',
            Findpw: { selector: 'find', xtype: 'find', autoCreate: true },

            JoinBtn: 'login button[action=join]',
            Join: { selector: 'join', xtype: 'join', autoCreate: true },

            LoginPanel:{selector:'memberlogin',xtype:'memberlogin',autoCreate:true},

            loginId: 'memberlogin #loginid',
            loginpwd: 'memberlogin #loginpwd'
        },
        control: {
            LoginBtn: { tap : 'goLogin' },
            FindpwBtn: { tap : 'goFindpw' },
            JoinBtn: { tap : 'goJoin' }
        }
    },


    encryptionMd5 : function (pwd) {
        var me = this;
        var md5 = me.getApplication().getController('md5_controller');
        return md5.hex_md5(pwd);
    },

    goLogin: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        cookie_ctrl.eraseCookie('LoginId');
        cookie_ctrl.eraseCookie('LoginUnum');
        cookie_ctrl.eraseCookie('LoginUname');

        var userId = this.getLoginId().getValue();
        var userPass = this.getLoginpwd().getValue();
        var md_pass = this.encryptionMd5(userPass);

        var postData = {
            loginId: userId,
            loginPwd: md_pass
        };

        var emailCheck = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

        if (userId.length < 6 || !emailCheck.test(userId)) {
            Ext.Msg.alert('알림',"아이디가 올바르지 않습니다.");
        }
        else if (userPass.length < 8) {
            Ext.Msg.alert('알림', "비밀번호가 올바르지 않습니다.");
        }
        else {
            Ext.Ajax.request({
                url: 'Login.php',
                params: postData,
                method: 'POST',
                success: function (response) {
                    var text = response.responseText;
                    if( text === 'queryError' ){
                        Ext.Msg.alert('알림', '해당정보가 없습니다.', Ext.emptyFn);
                    }
                    else if( text === 'PwdError' ){
                        Ext.Msg.alert('알림', '비밀번호가 올바르지 않습니다.', Ext.emptyFn);
                    }
                    else if( text === 'Success' ){
                        applyLogin(userId);
                    }
                }
            });

            function applyLogin(userId){
                var getvalueStore = Ext.getStore('getloginvaluestore');

                getvalueStore.getProxy().setExtraParams({
                    "userId" : userId
                });

                getvalueStore.load({callback: function(records, operation, success) {
                    if (success) {
                        var valueId = getvalueStore.getData().items[0].data.Uid;
                        var valueName = getvalueStore.getData().items[0].data.Uname;
                        var valueUnum = getvalueStore.getData().items[0].data.Unum;

                        cookie_ctrl.writeCookie('LoginId', valueId, 1);
                        cookie_ctrl.writeCookie('LoginUnum', valueUnum, 1);
                        cookie_ctrl.writeCookie('LoginUname', valueName, 1);



                        Ext.Msg.alert('알림', valueName+'님 환영합니다', Ext.emptyFn);

                        Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'up' });
                        Ext.Viewport.setActiveItem({ xtype: 'yogurt_main' })
                    }
                }, scope :this
                });

            }
        }

    },

    goFindpw: function () {
        Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'up' });
        Ext.Viewport.setActiveItem(this.getFindpw());
    },

    goJoin: function () {
        Ext.Viewport.getLayout().setAnimation({ type: 'slide', direction: 'up' });
        Ext.Viewport.setActiveItem(this.getJoin());
    }
});