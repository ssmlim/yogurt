var getChildSearchStore;
var getNoticeStore;
Ext.define('Yogurt.controller.Yogurt_MainController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            Main: { selector: 'yogurt_main', xtype: 'yogurt_main', autoCreate: true },
            LogOut: 'yogurt_main button[action=LogOut]',
            Login: {selector: 'login', xtype: 'login', autoCreate: true},

            UserEdit: 'yogurt_main button[action=UserEdit]',
            UserEditOk: 'useredit_form button[action=UserEditOk]',
            UserEditCancel: 'useredit_form button[action=UserEditCancel]',

            HelpBtn: 'yogurt_main button[action=Help]',
            Help: { selector: 'help', xtype: 'help', autoCreate: true },

            Notice : 'noticelist',
            NoticeMain : { selector: 'notice', xtype: 'notice', autoCreate: true },
            Searchchildview: { selector: 'searchchildview', xtype: 'searchchildview', autoCreate: true },
            NoticeDetail : { selector: 'noticedetail', xtype: 'noticedetail', autoCreate: true }
        },

        control: {
            HelpBtn : {tap : 'onHelp'},

            UserEdit: { tap: 'goUserEdit'},
            UserEditOk: { tap: 'onUserEditOk'},
            UserEditCancel: { tap: 'goHome' },

            LogOut: { tap: 'goLogOut' },
            Searchchildview : {
                initialize : 'onChildSearchSet',
                tap: 'onChildSearchDetail'

            },
            Notice : {
                initialize : 'onNoticeSet',
                itemtap : 'onNoticeDetail'
            }
        }
    },

    onHelp : function(btn, event) {
        this.getHelp().setCentered(true);
        this.getHelp().show();
    },


    onNoticeDetail : function (list, index, target, record, e, eOpts) {

        var Nnum = record.get('Nnum');

        getNoticeStore.getProxy().setExtraParams({
            "type": 'One',
            "Nnum" : Nnum
        });
        getNoticeStore.load();

        var html = "<table width ='100%'>" +
            "<tr><td width='30%'>작성자 :</td><td width='68%'>"+getNoticeStore.getData().items[0].data.Nname+"</td></tr>"+
            "<tr><td width='30%'>작성일 :</td><td width='68%'>"+getNoticeStore.getData().items[0].data.Ndate+"</td></tr>"+
            "<tr><td width='30%'>제목 :</td><td width='80%'>"+getNoticeStore.getData().items[0].data.Ntitle+"</td></tr>"+
            "<tr><td width='30%'>내용 :</td><td width='80%'>"+getNoticeStore.getData().items[0].data.Nnotice+"</td></tr>"+
            "</table>";

        this.getNoticeDetail().items.items[0].setTitle("공지사항");
        this.getNoticeDetail().items.items[1].setHtml(html);
        this.getNoticeDetail().setCentered(true);
        this.getNoticeDetail().show();
        this.onNoticeSet();
    },

    onNoticeSet : function() {
        getNoticeStore = Ext.getStore('getnoticestore');
        getNoticeStore.getProxy().setExtraParams({
            "type": 'Entire'
        });
        getNoticeStore.load({callback: function (records, operation, success) {
            if(success) {
                this.getNotice().setStore(getNoticeStore);
            }
        },scope:this
        });
    },

    onChildSearchDetail : function(e, t) {
        var targetId= e.innerItems[e.activeIndex].innerHtmlElement.dom.lastChild.id;
        var targetSnum = targetId.substr(8,targetId.length-8);

        getChildSearchStore.getProxy().setExtraParams({
            "type": 'One',
            "Snum" : targetSnum
        });
        getChildSearchStore.load();

        console.log(getChildSearchStore);
        var Snum = getChildSearchStore.getData().items[0].data.Snum;
        var html = "<table  width ='100%'>" +
            "<tr><td colspan='2' width ='100%'><img width='240px' height='240px' src='../../imageData/SearchChildPhoto/SearchChildPhoto_"+Snum+".jpg' /></td></tr>"+
            "<tr><td width='32%'>발생일자 :</td><td width='68%'>"+getChildSearchStore.getData().items[0].data.Sname+"</td></tr>"+
            "<tr><td width='32%'>아동이름 :</td><td width='68%'>"+getChildSearchStore.getData().items[0].data.Sdate+"</td></tr>"+
            "<tr><td width='32%'>발생위치 :</td><td width='68%'>"+getChildSearchStore.getData().items[0].data.Slocation+"</td></tr>"+
            "<tr><td width='32%'>연락처     :</td><td width='68%'>"+getChildSearchStore.getData().items[0].data.Sphone+"</td></tr>"+
            "<tr><td width='32%'>특이사항 :</td><td width='68%'>"+getChildSearchStore.getData().items[0].data.Snote+"</td></tr>"+
            "</table>";

        this.getNoticeDetail().items.items[0].setTitle(getChildSearchStore.getData().items[0].data.Sname+" 어린이를 찾습니다.");
        this.getNoticeDetail().items.items[1].setHtml(html);
        this.getNoticeDetail().setCentered(true);
        this.getNoticeDetail().show();
},

    onChildSearchSet : function() {

        getChildSearchStore = Ext.getStore('getchildsearchstore');
        getChildSearchStore.getProxy().setExtraParams({
            "type": 'Entire'
        });
        getChildSearchStore.load({callback: function (records, operation, success) {
            if(success){
            var StoreLength = getChildSearchStore.getData().length;
            var carousel = this.getSearchchildview();
            var items = [];
            for(var i = 0 ; i< StoreLength; i++){
                var html = "<table id='tableid_"+getChildSearchStore.getData().items[i].data.Snum+"'>" +
                    "<tr width='100%' valign=center>" +
                        "<td width='30%' >" +
                    "<center><img width='65px' height='65px' src='../../imageData/SearchChildPhoto/SearchChildPhoto_"+getChildSearchStore.getData().items[i].data.Snum+".jpg' /></center></td>"+
                        "<td width='70%'>" +
                            "<div>발생일자 : "+getChildSearchStore.getData().items[i].data.Sdate+"</div>" +
                            "<div>아동이름 : "+getChildSearchStore.getData().items[i].data.Sname+"</div>" +
                            "<div>발생위치 : "+getChildSearchStore.getData().items[i].data.Slocation+"</div>" +
                            "<div>  연락처   : "+getChildSearchStore.getData().items[i].data.Sphone+"</div>" +
                        "</td></tr></table>";
                items.push({
                    html : html
                });
                carousel.setItems(items);

            }
            carousel.setActiveItem(0);
            }
            else {
                this.getNoticeMain().items.items[0].hide();
                this.getNoticeMain().items.items[1].show();
            }

        },scope:this
        });
    },

    encryptionMd5 : function (pwd) {
        var me = this;
        var md5 = me.getApplication().getController('md5_controller');
        return md5.hex_md5(pwd);
    },

    goHome : function () {
        this.getMain().reset();
        this.getMain().getNavigationBar().query('button')[0].hide();
        this.getMain().getNavigationBar().query('button')[1].show();
        this.getMain().getNavigationBar().query('button')[2].show();
        this.getMain().getNavigationBar().query('button')[3].hide();
        this.getMain().getNavigationBar().query('button')[4].show();
    },




    goUserEdit: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var userId = cookie_ctrl.readCookie("LoginId");

        var getUserInfoStore = Ext.getStore('getuserinfostore');

        getUserInfoStore.getProxy().setExtraParams({
            "userId": userId
        });

        getUserInfoStore.load({callback: function (records, operation, success) {
            if (success) {
                var getId = getUserInfoStore.getData().items[0].data.Uid;
                var getName = getUserInfoStore.getData().items[0].data.Uname;
                var getUphone = getUserInfoStore.getData().items[0].data.Uphone;

                this.getMain().push({
                    xtype: 'useredit_form'
                });

                this.getMain().getNavigationBar().query('button')[0].show();
                this.getMain().getNavigationBar().query('button')[1].hide();
                this.getMain().getNavigationBar().query('button')[2].hide();
                this.getMain().getNavigationBar().query('button')[4].hide();

                Ext.getCmp('editId').setValue(getId);
                Ext.getCmp('editName').setValue(getName);
                Ext.getCmp('editPhone').setValue(getUphone);

            }
        }, scope: this
        });
    },


    onUserEditOk: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        var cookieUnum = cookie_ctrl.readCookie("LoginUnum");

        var getPhone = Ext.getCmp('editPhone').getValue();
        var getPwd = Ext.getCmp('editPassword').getValue();
        var getNewpwd = Ext.getCmp('editNewpassword').getValue();
        var getNewpwdck = Ext.getCmp('editNewpasswordcheck').getValue();

        if (getPhone == '') {
            Ext.Msg.alert('알림', '휴대폰 번호를 입력해주세요', Ext.emptyFn);
        }
        else if (!(getNewpwd == getNewpwdck)) {
            Ext.Msg.alert('알림', '변경할 비밀번호를 확인해주세요.', Ext.emptyFn);
        }
        else if (getPwd == '') {
            Ext.Msg.alert('알림', '비밀번호를 입력해주세요.', Ext.emptyFn);
        }
        else {

            var md5_getpwd = this.encryptionMd5(getPwd);
            var chkParams = {
                'Unum': cookieUnum,
                'Upwd': md5_getpwd
            };

            var params = {};

            Ext.Ajax.request({
                url: 'CheckUserPwd.php',
                params: chkParams,
                method: 'POST',
                success: function (response) {
                    var text = response.responseText;
                    if (text === 'queryError') {
                        Ext.Msg.alert('알림', '해당정보가 없습니다.', Ext.emptyFn);
                    }
                    else if (text === 'PwdError') {
                        Ext.Msg.alert('알림', '비밀번호가 올바르지 않습니다.', Ext.emptyFn);
                    }
                    else if (text === 'Success') {
                        if (!((getNewpwd == '') && (getNewpwdck == ''))) {

                            if (getNewpwd.length < 8) {
                                Ext.Msg.alert('알림', '비밀번호는 8자리 이상으로 해주세요.', Ext.emptyFn);
                            }
                            else {
                                var md5_getnewpwd = this.encryptionMd5(getNewpwd);
                                params = { getUnum: cookieUnum, getUphone: getPhone, getUpwd: md5_getnewpwd };
                                if (!(params == {})) {
                                    Ext.Ajax.request({
                                        url: 'UserEdit.php',
                                        params: params,
                                        success: function (response) {
                                            Ext.Msg.alert('알림', '회원정보를 변경하였습니다.', Ext.emptyFn);
                                        }, scope: this
                                    });
                                }
                            }

                        }
                        else {
                            params = { getUnum: cookieUnum, getUphone: getPhone, getUpwd: '' };
                            if (!(params == {})) {
                                Ext.Ajax.request({
                                    url: 'UserEdit.php',
                                    params: params,
                                    success: function (response) {
                                        Ext.Msg.alert('알림', '회원정보를 변경하였습니다.', Ext.emptyFn);
                                    }, scope: this
                                });
                            }
                        }


                    }
                }, scope: this
            });
        }
    },

    goLogOut: function () {
        var me = this;
        var cookie_ctrl = me.getApplication().getController('Cookie');
        cookie_ctrl.eraseCookie('LoginId');
        cookie_ctrl.eraseCookie('LoginUnum');
        cookie_ctrl.eraseCookie('LoginUname');

        Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
        Ext.Viewport.show({ type: 'pop'});

        Ext.Viewport.add({ xtype: 'login' })



    }
});