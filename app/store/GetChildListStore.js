Ext.define('Yogurt.store.GetChildListStore', {
    extend: 'Ext.data.Store',
    xtype : 'childliststore',
    config: {
        autoLoad: false,
        storeId : 'childliststore',
        model: 'Yogurt.model.GetChildListModel',
        proxy: {
            type: 'ajax',
            url: 'getChildList.php',
            reader: {
                type: 'json'
            },
            extraParams:{
                Unum : '1'
            }
        }
    }
});