Ext.define('Yogurt.store.GetChildHistoryCountStore', {
    extend: 'Ext.data.Store',
    xtype : 'getchildhistorycountstore',
    config: {
        autoLoad: false,
        storeId : 'getchildhistorycountstore',
        model: 'Yogurt.model.GetChildHistoryCountModel',
        proxy: {
            type: 'ajax',
            url: 'getChildHistoryCount.php',
            reader: {
                type: 'json'
            },
            extraParams:{
                Unum : '1'
            }
        }
    }
});