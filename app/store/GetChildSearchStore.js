Ext.define('Yogurt.store.GetChildSearchStore', {
    extend: 'Ext.data.Store',
    xtype: 'getchildsearchstore',
    config: {
        model: 'Yogurt.model.GetChildSearchModel',
        storeId : 'getchildsearchstore',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url : 'getChildSearch.php',
            extraParams : {
                type : '',
                Snum : ''
            },
            reader: {
                type: 'json'
            }
        }
    }
});