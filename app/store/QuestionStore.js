Ext.define('Yogurt.store.QuestionStore', {
    extend: 'Ext.data.Store',
    xtype: 'questionstore',
    config: {
        autoLoad: false,
        model: 'Yogurt.model.QuestionModel',
        storeId : 'questionstoreId',
        proxy: {
            type: 'ajax',
            url : 'getQuestion.php',
            extraParams:{
                "Qnum":"1",
                "Qtype" : '1'
            },
            reader: {
                type: 'json'
            }
        }
    }
});