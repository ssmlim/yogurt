Ext.define('Yogurt.store.GetNonAnswerStore', {
    extend: 'Ext.data.Store',
    xtype: 'getnonanswerstore',
    config: {
        autoLoad: false,
        storeId : 'getnonanswerstore',
        model: 'Yogurt.model.GetNonAnswerModel',
        proxy: {
            type: 'ajax',
            url : 'getNonanswer.php',
            extraParams:{
               "Anum": "1",
               "Unum":"1",
               "Cnum":"1"
            },
            reader: {
                type: 'json'
            }
        }
    }
});

