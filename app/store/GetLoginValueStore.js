Ext.define('Yogurt.store.GetLoginValueStore', {
    extend: 'Ext.data.Store',
    xtype: 'getloginvaluestore',
    config: {
        model: 'Yogurt.model.GetLoginValueModel',
        storeId : 'getloginvaluestore',
        autoLoad: false,
        extraParams:{
            userId : '1'
        },
        proxy: {
            type: 'ajax',
            url : 'getLoginValue.php',
            reader: {
                type: 'json'
            }
        }
    }
});