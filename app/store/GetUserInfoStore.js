Ext.define('Yogurt.store.GetUserInfoStore', {
    extend: 'Ext.data.Store',
    xtype: 'getuserinfostore',
    config: {
        model: 'Yogurt.model.GetUserInfoModel',
        storeId : 'getuserinfostore',
        autoLoad: false,
        extraParams:{
            userId : '1'
        },
        proxy: {
            type: 'ajax',
            url : 'getUserInfo.php',
            reader: {
                type: 'json'
            }
        }
    }
});