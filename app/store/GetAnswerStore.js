Ext.define('Yogurt.store.GetAnswerStore', {
    extend: 'Ext.data.Store',
    xtype: 'getanswerstore',
    config: {
        autoLoad: false,
        storeId : 'getanswerstore',
        model: 'Yogurt.model.GetAnswerModel',
        groupField: 'Qpart',
        grouper: {
            sortProperty: 'QpartPriority',
            groupFn: function (record) {
                return record.get('QpartPriority');
            }
        },
        proxy: {
            type: 'ajax',
            url : 'getAnswer.php',
            extraParams:{
                "Anum": "1",
                "Unum":"1",
                "Cnum":"1",
                "Qtype" : "NO"
            },
            reader: {
                type: 'json'
            }
        }
    }
});

