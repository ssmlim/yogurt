Ext.define('Yogurt.store.GetChildHistoryStore', {
    extend: 'Ext.data.Store',
    xtype : 'getchildhistorystore',
    config: {
        autoLoad: false,
        storeId : 'getchildhistorystore',
        model: 'Yogurt.model.GetChildHistoryModel',
        proxy: {
            type: 'ajax',
            url: 'getChildHistory.php',
            reader: {
                type: 'json'
            },
            extraParams:{
                Unum : '1',
                Cnum : '1'
            }
        }
    }
});