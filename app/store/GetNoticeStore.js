Ext.define('Yogurt.store.GetNoticeStore', {
    extend: 'Ext.data.Store',
    xtype: 'getnoticestore',
    config: {
        autoLoad: false,
        storeId : 'getnoticestore',
        model: 'Yogurt.model.GetNoticeModel',
        proxy: {
            type: 'ajax',
            url : 'getNotice.php',
            extraParams : {
                type : '',
                Nnum : ''
            },
            reader: {
                type: 'json'
            }
        }
    }
});

