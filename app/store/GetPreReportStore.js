Ext.define('Yogurt.store.GetPreReportStore', {
    extend: 'Ext.data.Store',
    xtype: 'getprereportstore',
    config: {
        model: 'Yogurt.model.GetPreReportModel',
        storeId : 'getprereportstore',
        autoLoad: false,
        extraParams:{
            Unum : '1',
            Cnum : '1',
            Rtype : '1'
        },
        proxy: {
            type: 'ajax',
            url : 'getPreReport.php',
            reader: {
                type: 'json'
            }
        }
    }
});