Ext.define('Yogurt.model.GetLoginValueModel', {
    extend: 'Ext.data.Model',
    xtype: 'getloginvaluemodel',
    config : {
        fields: ['Unum','Uid','Uname']
    }

});