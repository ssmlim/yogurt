Ext.define('Yogurt.model.GetChildHistoryCountModel', {
    extend: 'Ext.data.Model',
    xtype: 'childhistorycountmodel',
    config : {
        fields: ['Unum','Cnum','ChildName','ReportCount']
    }
});
