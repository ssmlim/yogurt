Ext.define('Yogurt.model.GetNoticeModel', {
    extend: 'Ext.data.Model',
    xtype: 'getnoticemodel',
    config : {
        fields: ['Nnum','Ntitle','Nname','Ndate','Nnotice']
    }

});