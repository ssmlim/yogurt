Ext.define('Yogurt.model.GetAnswerModel', {
    extend: 'Ext.data.Model',
    config : {
        fields: ['Aid','Qpart','Qnum','Qask','Aanswer','QpartPriority']
    }

});
