Ext.define('Yogurt.model.GetPreReportModel', {
    extend: 'Ext.data.Model',
    config : {
        fields: ['result', 'Qtype', 'questionCount', 'curAnum', 'NonCompleteAnum', 'NonCompleteQnum']
    }

});
