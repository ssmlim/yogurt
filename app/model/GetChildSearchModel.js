Ext.define('Yogurt.model.GetChildSearchModel', {
    extend: 'Ext.data.Model',
    xtype: 'getchildsearchmodel',
    config : {
        fields: ['Snum','Sdate','Sname','Slocation','Sphone','Snote']
    }

});