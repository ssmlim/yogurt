Ext.define('Yogurt.model.GetUserInfoModel', {
    extend: 'Ext.data.Model',
    xtype: 'getuserinfomodel',
    config : {
        fields: ['Unum','Uid','Uname', 'Uphone']
    }

});