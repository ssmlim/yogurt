Ext.define('Yogurt.model.GetChildListModel', {
    extend: 'Ext.data.Model',
    xtype: 'childlistmodel',
    config : {
        fields: ['Cnum','Unum','ChildName', 'ChildBirth','ChildSex']
    }

});
