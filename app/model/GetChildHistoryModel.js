Ext.define('Yogurt.model.GetChildHistoryModel', {
    extend: 'Ext.data.Model',
    xtype: 'childhistorymodel',
    config : {
        fields: ['RowNumber','Anum','Adate','Unum','Cnum','Qtype','ChildName']
    }
});
