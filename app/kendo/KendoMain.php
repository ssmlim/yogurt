<!DOCTYPE html public>
<html>
<head>

    <meta http-equiv="content-type" content="charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel=stylesheet href="http://cyberco.cafe24.com/Yogurt/app/kendo/style/kendo.common.min.css" />
    <link rel=stylesheet href="http://cyberco.cafe24.com/Yogurt/app/kendo/style/kendo.default.min.css" />
    <link rel=stylesheet href="http://cyberco.cafe24.com/Yogurt/app/kendo/style/kendo.dataviz.default.min.css" />
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/jquery.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.core.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.dataviz.core.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.dataviz.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.dataviz.chart.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.dataviz.canvas.min.js"></script>
    <script src="http://cyberco.cafe24.com/Yogurt/app/kendo/kendo.culture.ko.min.js"></script>

    <script type="text/javascript">
        //set current to the "en-GB" culture script
        kendo.culture("ko");
    </script>

    <style>
        .k-chart { height : 100% ; width : 100%}
        .innerhtml { height : 100% }
        #body { margin : 0 }
    </style>


</head>
<script>
</script>
<body id = "body">

<div id=body></div><noscript>This web application requires JavaScript enabled</noscript></body>
<div id="chart"></div>
<script>


    $("#chart").kendoChart({tooltip: {visible: true,format: "{0:C}",template: "#= series.name #<br /> #= value #"},
        theme: "default",seriesDefaults: {type: "line",stack: false},



        dataSource: {
            transport: {
                read: function(options) {
                    var jsonData = {Unum:'<?php echo $_REQUEST['Unum']; ?>',Cnum:'<?php echo$_REQUEST['Cnum']; ?>',Anum:'<?php echo $_REQUEST['Anum']; ?>',Qtype:'<?php echo $_REQUEST['Qtype']; ?>'};
                    $.ajax({
                        url: "getQuestionResult.php",
                        data:  jsonData,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            options.success(result);
                        }
                    });
                }

            },
            schema: {
                model: {
                    fields: {
                        category: { type: "string" },
                        No1: { type: "int" }
                    }
                }
            }
        },
        series: [
            {
                field: "No1",
                name: "",
                markers : {visible : true},
                labels: {
                    visible: true
                },
                color : 'gray'
            }

        ],

        dataBound: function(e) {
            var from = e.sender.dataSource._pristine[0].Minvalue;
            var to = e.sender.dataSource._pristine[0].Maxvalue;

            var date1 = e.sender.dataSource._pristine[0].Date1;
            var Y_Max = e.sender.dataSource._pristine[0].Y_Max;

            var Qtype = e.sender.dataSource._pristine[0].type;

            if(Qtype=='NO'){
                e.sender.options.valueAxis.plotBands[0].from = 0 ;
                e.sender.options.valueAxis.plotBands[0].to = 0;
            }
            else {
                e.sender.options.valueAxis.plotBands[0].from = from ;
                e.sender.options.valueAxis.plotBands[0].to = to;
            }


            e.sender.options.valueAxis.max = Y_Max;
            e.sender.options.series[0].name = date1;

        },

        categoryAxis: {
            field: "category"
        },
        tooltip: {visible: true, format: "{0}"}
        ,valueAxis: {
            labels: {
                format: "{0}"
            },
            majorUnit: 10,
            plotBands: [{
                from: '0',  // MIN 값
                to: '0',  // MAX 값
                color: "#c00",
                opacity: 0.3
            }],
            min: 0, max: 30
        }
        ,legend: {position: "top"}
        ,seriesClick: function(e) {ajaxRequest(fmTest.UniHTMLFrame_Kendo_volt, "Click", ["name="+e.series.name, "category="+e.category, "value="+((typeof e.value==='object')?e.value.x+","+e.value.y:e.value)]

        );

        }
    });

</script>
</html>