/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src'
});
//</debug>

Ext.application({
    name: 'Yogurt',

    requires: [
        'Ext.MessageBox'
    ],

    models: [
        'GetLoginValueModel',
        'GetUserInfoModel',
        'GetChildListModel',
        'GetChildHistoryModel',

        'GetChildHistoryCountModel',
        'QuestionModel',
        'GetPreReportModel',
        'GetNonAnswerModel',

        'GetAnswerModel',

        'GetChildSearchModel',
        'GetNoticeModel'
    ],

    stores: [
        'GetLoginValueStore',
        'GetUserInfoStore',
        'GetChildListStore',
        'GetChildHistoryStore',

        'GetChildHistoryCountStore',
        'QuestionStore',
        'GetPreReportStore',
        'GetNonAnswerStore',

        'GetAnswerStore',

        'GetChildSearchStore',
        'GetNoticeStore'
    ],

    views: [
        'Help',
        'common.HelpPanel',

        'Login',
        'common.MemberLogin',

        'Join',
        'common.MemberJoin',

        'Find',
        'common.Find_Password',

        'Yogurt_Main',
        'common.Yogurt_MainPanel',
        'common.Yogurt_MainTestSelect',

        'common.Notice',
        'common.NoticeList',
        'common.NoticeDetail',
        'common.SearchChildView',

        'common.UserEdit_Form',

        'ChildInfo',
        'common.ChildInfo_List',

 
        'common.ChildDetail_Form',

        'common.ChildAdd_Form',

        'ChildHistory',
        'common.ChildHistory_list',
        'common.ChildHistory_btn',

        'YogurtChild_Chart',
        'common.YogurtChild_ChartView',

        'YogurtChild_Main',
        'common.YogurtChild_TestChildList',
        'common.YogurtChild_TestSelect',

        'YogurtChild_Test',
        'common.YogurtChild_TestMain',
        'common.YogurtChild_TestQuestion',
        'common.YogurtChild_TestButton',

        'YogurtChild_NonAnswer',
        'common.YogurtChild_NonAnswerList',

        'YogurtChild_Result',
        'common.YogurtChild_ResultView',

        'common.AnswerListView'

    ],

    controllers: [
        'LoginController',
        'JoinController',
        'FindController',
        'Cookie',
        'ChildInfoController',
        'Yogurt_MainController',
        'YogurtChild_MainController',
        'YogurtChild_TestController',
        'md5_controller'
    ],
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('Yogurt.view.Login'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
