Ext.define('Yogurt.view.common.Result_Chart_3D', {
    extend: 'Ext.chart.series.Bar',
    requires: ['Yogurt.view.common.sprite.Column3D'],
    seriesType: 'columnSeries3d',
    alias: 'series.column3d',
    type: 'column3d',
    config: {
        itemInstancing: null
    }
});