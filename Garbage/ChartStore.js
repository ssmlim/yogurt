Ext.define('Yogurt.store.ChartStore', {
    extend: 'Ext.data.Store',
    xtype: 'chartstore',
    config: {
        model: 'Yogurt.model.ChartModel',
        data: [
            { name: '신체', data1: 24, data2: 60 },// 변경시 차트 X축 항목 변경됨
            { name: '행동', data1: 20, data2: 70 }
        ],
        reader: {
            type: 'json'
        }
    }
});