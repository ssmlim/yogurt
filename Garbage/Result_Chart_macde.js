chartStore = Ext.create('Yogurt.store.ChartStore', {});

// perhatikan properti gradient dari items milik panel
var colors = ['url(#v-2)', 'url(#v-3)'];

Ext.define('Yogurt.view.common.Result_Chart', {
    extend: 'Ext.chart.Panel',
    xtype: 'result_chart',
    //style: 'background-color: #FFFFFF;',
    config: {
        layout: 'fit',
        fullscreen: true,
        items: {
            cls: 'column1',
            theme: 'Demo',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            store: chartStore,
            shadow: true,
            gradients: [
                {
                    'id': 'v-2',
                    'angle': 0,
                    stops: {
                        0: { color: 'rgb(180, 216, 42)' },
                        100: { color: 'rgb(94, 114, 13)' }
                    }
                },
                {
                    'id': 'v-3',
                    'angle': 0,
                    stops: {
                        0: { color: 'rgb(43, 221, 115)' },
                        100: { color: 'rgb(14, 117, 56)' }
                    }
                }
            ],

            axes: [
                {
                    title: 'Y축 타이틀',
                    type: 'Numeric',
                    position: 'left',
                    fields: ['data1'],
                    minimum: 0,
                    maximum: 100,
                    label: {
                        renderer: function(v) {
                            return v.toFixed(0);
                        }
                    }
                },
                {
                    title: 'X축 타이틀',
                    type: 'Category',
                    position: 'bottom',
                    fields: ['name']
                }
            ],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                renderer: function(sprite, storeItem, barAttr, i, store) {
                    barAttr.fill = colors[i % colors.length];
                    return barAttr;
                },// 막대그래프 색상 랜더
                label: {
                    field: 'data1'
                },
                xField: 'name',
                yField: 'data1'
            }],
            interactions: [{
                type: 'panzoom',
                axes: ['bottom']
            }]
        }
    }
});